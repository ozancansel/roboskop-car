#include "bluetoothutil.h"
#include <QDebug>
#include <QtQml>
#include <QTime>

void BluetoothUtil::init(){
    qRegisterMetaType<BluetoothUtil::BtState>("bluetooth::BtState");
    qmlRegisterType<BluetoothUtil>("BluetoothUtil" , 1 , 0 , "BluetoothUtil");
}

BluetoothUtil::BluetoothUtil()
{

    _socket = new QBluetoothSocket(QBluetoothServiceInfo::RfcommProtocol, this);
    _agent = new QBluetoothDeviceDiscoveryAgent(this);

    _commandQueued = false;
    _senderTimer.setInterval(30);
    _randomTimer.setInterval(3000);
    _state = None;

    connect(_socket , SIGNAL(connected()) , this , SLOT(btConnected()));
    connect(_socket , SIGNAL(disconnected()) , this , SLOT(btDisconnected()));
    connect(_agent , SIGNAL(deviceDiscovered(QBluetoothDeviceInfo)) , this , SLOT(btDeviceDiscovered(QBluetoothDeviceInfo)));
    connect(_agent , SIGNAL(finished()) , this , SLOT(btScanFinished()));
    connect(_socket , SIGNAL(stateChanged(QBluetoothSocket::SocketState)) , this , SLOT(btStateChanged(QBluetoothSocket::SocketState)));
    connect(&_senderTimer , SIGNAL(timeout()) , this , SLOT(sendTimerTick()));
    connect(&_randomTimer , SIGNAL(timeout()) , this , SLOT(randomTimerTick()));


    setDebugEnabled(false);
}

void BluetoothUtil::btConnected(){

    if(_debug)
        qDebug() << "Bluetooth bağlandı.";

    _state = Connected ;

    if(!_senderTimer.isActive())
        _senderTimer.start();

    emit    stateChanged();
}

void BluetoothUtil::btDisconnected(){

    if(_debug)
        qDebug() << "Bluetooth bağlantısı koptu.";

    _state = Disconnected;

    emit  stateChanged();
}

//Private slots
void BluetoothUtil::btDeviceDiscovered(const QBluetoothDeviceInfo &device){
    qDebug() << device.name() << " " << device.address() << " discovered" << " Service : ";

    emit deviceDiscovered(device.name() , device.address().toString());
}

void BluetoothUtil::btScanFinished(){

    if(_state == Discovering)
        _state = None ;

    if(debug())
        qDebug() << "Bluetooth tarama bitti";

    emit scanStatusChanged();
}

//Public slots
void BluetoothUtil::connectToBt(QString address){

    if(debug())
        qDebug() << address << "'a bağlanılıyor.";

    _agent->stop();

    //Eğer soket açıksa kapatılıyor
    if(_socket->isOpen())
        _socket->close();

    _socket->connectToService(QBluetoothAddress(address) , QBluetoothUuid(QBluetoothUuid::SerialPort));

}

void BluetoothUtil::startScan(){

    if(debug())
        qDebug() << "Tarama başladı";

    _state = Discovering ;

    _agent->stop();
    _agent->start();

    emit    stateChanged();
}

bool BluetoothUtil::debug(){
    return _debug;
}

void BluetoothUtil::send(QString text){

    if(debug())
        qDebug() << text << " gönderiliyor.";

    int length = _buffer.length();

    if(_buffer.empty() && QTime::currentTime().msec() - _lastTick.msec() > _senderTimer.interval()){
        qDebug() << "Not buffer";
        _socket->write(text.toLatin1());

        return;
    }   else    {
        qDebug() << "Buffering";

        if(!_senderTimer.isActive())
            _senderTimer.start();

        _buffer.enqueue(text);
    }
}

void BluetoothUtil::setDebugEnabled(bool enabled){
    _debug = enabled;
}

void BluetoothUtil::sendTimerTick(){

    //Eğer ki buffer boşsa timer durduruluyor
    if(_buffer.isEmpty()){
        _senderTimer.stop();
        return;
    }

    //Eğer ki bağlantı yoksa, buffer temizleniyor
    if(!_socket->isOpen()){

        _senderTimer.stop();
        _buffer.clear();

        return;
    }

    QString command = _buffer.dequeue();

    _socket->write(command.toLatin1());

    _lastTick = QTime::currentTime();
}

void BluetoothUtil::randomTimerTick(){


    int val = qrand() % 5;

    if(_debug)
        qDebug() << "Random state : " << val ;

    _state = (BtState)val;

    emit stateChanged();
}

void BluetoothUtil::setInterpreter(SerialCommandInterpreter *intepreter){
    intepreter->listen(_socket);
}

void BluetoothUtil::closeSocket(){

    if(_debug)
        qDebug() << "Bluetooth bağlantısı kesiliyor";

    _socket->close();
}

BluetoothUtil::BtState  BluetoothUtil::getState(){
    return _state;
}

void BluetoothUtil::btStateChanged(QBluetoothSocket::SocketState state){

    if(state == QBluetoothSocket::UnconnectedState && (_state != Disconnected || _state != Error)){
        _state = None;
    }else if(state == QBluetoothSocket::ConnectingState){
        _state = Connecting;
    }else if(state == QBluetoothSocket::ConnectedState){
        _state = Connected;
    }

    if(_debug)
        qDebug() << "Soket durumu değişti : " << state;

    emit    stateChanged();
}

void BluetoothUtil::startRandomState(){
    _randomTimer.start();
}

int BluetoothUtil::getBufferedCommandsLength(){
    return _buffer.length();
}
