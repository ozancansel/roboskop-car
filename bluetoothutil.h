#ifndef BLUETOOTHUTIL_H
#define BLUETOOTHUTIL_H

#include <QObject>
#include <QBluetoothSocket>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothLocalDevice>
#include <QList>
#include <QTimer>
#include <QTime>
#include <QQueue>
#include <QQuickItem>
#include "serialcommandinterpreter.h"
#include "iroboskopcommandinterpreter.h"

class BluetoothUtil : public  QQuickItem
{

    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_ENUMS(BtState)
    Q_PROPERTY(BtState state READ getState NOTIFY stateChanged)
    Q_PROPERTY(int bufferedCommandsLength READ getBufferedCommandsLength)

public:

    enum BtState{ Disconnected = 0, Connecting = 1 , Connected = 2, Discovering = 3 , Error = 4 , None = 5};

    static void init();
    BluetoothUtil();
    bool        connecting();
    bool        connected();
    bool        scanning();
    void        setDebugEnabled(bool enabled = true);
    void        setInterpreter(SerialCommandInterpreter *intepreter);
    void        startRandomState();
    BtState     getState();
    int         getBufferedCommandsLength();

signals:

    void        deviceDiscovered(QString name , QString address);
    void        connectionStatusChanged();
    void        connectingStatusChanged();
    void        scanStatusChanged();
    void        dataIncome(QString data);
    void        stateChanged();

public slots:

    void        connectToBt(QString address);
    void        closeSocket();
    void        startScan();
    void        send(QString text);

private slots:

    void        btConnected();
    void        btDisconnected();
    void        btDeviceDiscovered(const QBluetoothDeviceInfo&);
    void        btScanFinished();
    void        sendTimerTick();
    void        randomTimerTick();
    void        btStateChanged(QBluetoothSocket::SocketState state);

private:

    bool            _debug;
    bool            _commandQueued;

    BtState          _state;

    QMap<QString , QBluetoothDeviceInfo*>       _discoveredDevices;
    QQueue<QString>                             _buffer;
    QBluetoothDeviceDiscoveryAgent      *_agent;
    QBluetoothLocalDevice               *_localDevice;
    QBluetoothSocket                    *_socket;
    QTimer                              _senderTimer;
    QTimer                              _randomTimer;
    QTime                               _lastTick;

    bool            debug();
};

#endif // BLUETOOTHUTIL_H
