#include "globalnotifier.h"
#include <QDebug>

GlobalNotifier::GlobalNotifier()
{

}

void GlobalNotifier::notify(QString name, QStringList notifyArgs){

    if(_debug)
        qDebug() << "Broadcast : " << name << " Args : " << notifyArgs;

    emit broadcast(name , notifyArgs);
}

void GlobalNotifier::setDebug(bool enabled){
    _debug = enabled;
}
