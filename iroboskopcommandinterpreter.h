#ifndef IROBOSKOPCOMMANDINTERPRETER_H
#define IROBOSKOPCOMMANDINTERPRETER_H

#include <QIODevice>

class IRoboskopCommandInterpreter
{

public:

    IRoboskopCommandInterpreter();
    virtual void    listen(QIODevice *device) = 0;

};

#endif // IROBOSKOPCOMMANDINTERPRETER_H
