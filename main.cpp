#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "bluetoothutil.h"
#include "serialcommandinterpreter.h"
#include "robogezginconfig.h"


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;
    QQmlContext *context = engine.rootContext();

    BluetoothUtil               bt;
    SerialCommandInterpreter    interpreter;

    bt.setInterpreter(&interpreter);
    bt.setDebugEnabled(true);

    RoboGezginConfig::registerQmlType();

    context->setContextProperty("bluetooth" , &bt);
    context->setContextProperty("commandInterpreter" , &interpreter);
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
