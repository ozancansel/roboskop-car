import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.0
import GezginBot 1.0
import "qml/control"
import "qml"
import "qml/form"

ApplicationWindow {
    visible         :   true
    visibility      :   Window.FullScreen
    title           :   "Roboskop Araba"

    RoboGezginConfig{
        id      :   settings
    }


    LineFollowing{
        id          :   lineFollowing
        onToastMessageRequired  :   {
            toast.displayMessage(message)
        }
        visible     :   false
    }


    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: footerBar.currentIndex
        anchors.top     :   parent.top
        height      :   parent.height - footerBar.height
        width       :   parent.width

        GezginPov{
            id          :   pov
            config      :   settings
            onToastMessageRequired  :   {
                toast.displayMessage(message)
            }
            onSetInteractive    :   swipeView.interactive   =   val
            onNeedConnection    :   swipeView.setCurrentIndex(2)
        }

        GezginBotSettings{
            config              :   settings
            onSensorAddRequired :   {
                //<sen>
                pov.addSensor(sen)
            }
            onToastMessageRequired  :   {
                toast.displayMessage(message)
            }
        }

        BluetoothDialog {
            id              :   bluetoothDialog
            onConnected     :   {
                swipeView.setCurrentIndex(0)
            }
            onDisconnected  :   {
                swipeView.setCurrentIndex(2)
            }
        }
    }

    Toast{
        id              :   toast
        container       :   swipeView.currentItem
        fillColor       :   "#191919"
        textColor       :   "white"
    }


    footer:     TabBar {
        id              :   footerBar
        currentIndex    :   swipeView.currentIndex
        position        :   TabBar.Footer


        TabButton{
            text    :   "Sürüş"
        }

        TabButton{
            text    :   "Ayarlar"
        }

        TabButton{
            text    :   "Bluetooth"
        }
    }
}
