import QtQuick 2.0
import QtSensors 5.3
import GezginBot 1.0
import "util"

Accelerometer{

    property RoboGezginConfig       config

    readonly property real zMax     :   calcZMax()
    readonly property real yMax     :   calcYMax()

    function calcZMax(){
        return 9.2
    }

    function calcYMax(){
        return 18.5 * (1 - (config.sensivity / 100))
    }

    function calcParameters(x , y , z){

        var clippedZ = z , clippedY = y


        if(y > yMax)
            clippedY = yMax
        else if(y < -yMax)
            clippedY = -yMax

        if(z > zMax)
            clippedZ = zMax
        else if(z < -zMax)
            clippedZ = -zMax

        var calculatedThrottle , calculatedSteering

        if(clippedZ > 0){
            calculatedThrottle = NumericHelper.map(clippedZ, 0 , zMax , 0 , config.maxForwardThrottle)
        }
        else{
            calculatedThrottle = NumericHelper.map(clippedZ , 0 , -zMax , 0 , -config.maxRearThrottle )
        }

        calculatedSteering = NumericHelper.map(clippedY , -yMax , yMax , -config.maxSteering , config.maxSteering)

        return { throttle : calculatedThrottle , steering : calculatedSteering }
    }

    id              :   accel
    dataRate        :   100

    signal throttleChanged(int th , int steering)

    onReadingChanged    :   {

        var calculated = calcParameters(accel.reading.x , accel.reading.y , accel.reading.z)

        throttleChanged(calculated.throttle , calculated.steering)
    }
}
