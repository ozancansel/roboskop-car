import QtQuick 2.0

Item {

    readonly property real  textHeight  :   0.3

    Column{

        anchors.fill    :   parent

        Item{
            width      :   parent.width
            height     :   parent.height * 0.3

            Text {
                id      :   xStdText
                text    :   qsTr("xStd")
                font.pixelSize  :   parent.height * textHeight
                anchors.centerIn    :   parent
                color      :    "white"
            }
        }

        Item{
            width      :   parent.width
            height     :   parent.height * 0.3

            Text {
                id      :   yStdText
                text    :   qsTr("yStd")
                font.pixelSize  :   parent.height * textHeight
                anchors.centerIn    :   parent
                color      :    "white"
            }
        }

        Item{
            width      :   parent.width
            height     :   parent.height * 0.3

            Text {
                id      :   zStdText
                text    :   qsTr("zStd")
                font.pixelSize  :   parent.height * textHeight
                anchors.centerIn    :   parent
                color      :    "white"
            }
        }
    }

    Connections {
        target              :   commandInterpreter
        onCommandIncome     :   {
            if(code === 103){
                var     xStd = parseInt(commandArgs[0])
                var     yStd = parseInt(commandArgs[1])
                var     zStd = parseInt(commandArgs[2])

                xStdText.text = xStd
                yStdText.text = yStd
                zStdText.text = zStd
            }
        }
    }

}
