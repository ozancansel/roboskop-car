import QtQuick 2.0

Rectangle {

    property bool   collapsed               :   false
    property int    expandedMagnitude       :   200
    property int    collapsedMagnitude      :   collapseButton.width
    property int    orientation             :   Qt.Vertical
    property int    buttonPosition          :   Qt.TopRightCorner
    property real   animationVelocity       :   3.5
    property int    collapseButtonRotation  :   0


    id          :   collapsiblePanel
    color       :   "transparent"

    Behavior on height  {
        SmoothedAnimation{
            velocity        :   expandedMagnitude * animationVelocity
        }
    }

    Behavior on width{
        SmoothedAnimation{
            velocity        :   expandedMagnitude * animationVelocity
        }
    }

    Item    {
        id                  :   collapseButton
        width               :   collapsedMagnitude
        height              :   collapsedMagnitude
        anchors.left        :   buttonPosition === Qt.TopLeftCorner || buttonPosition === Qt.BottomLeftCorner   ?   parent.left : undefined
        anchors.right       :   buttonPosition === Qt.TopRightCorner || buttonPosition === Qt.BottomRightCorner ? parent.right : undefined
        anchors.bottom      :   buttonPosition === Qt.BottomLeftCorner || buttonPosition === Qt.BottomRightCorner ? parent.bottom : undefined
        anchors.top         :   buttonPosition === Qt.TopLeftCorner || buttonPosition == Qt.TopRightCorner ? parent.top : undefined
        z                   :   9999

        Image   {
            id                  :   icon
            anchors.fill        :   parent
            anchors.margins     :   5
            source              :   collapsed ? "/res/img/expand-48x48.png" : "/res/img/collapse-48x48.png"
            fillMode            :   Image.PreserveAspectFit
            rotation            :   collapseButtonRotation
        }

        MouseArea{
            id             :   area
            anchors.fill   :   parent
            onClicked      :   collapsed = !collapsed
        }
    }

    Component.onCompleted: {
        if(orientation === Qt.Vertical){
            collapsiblePanel.width   =   Qt.binding(function(){
                var calculated = collapsed ? collapsedMagnitude : expandedMagnitude
                console.log("Height : " + collapsiblePanel.height)
                return collapsed ? collapsedMagnitude : expandedMagnitude })
        }
        else
            collapsiblePanel.height  =   Qt.binding(function() { return collapsed ? collapsedMagnitude : expandedMagnitude})
    }
}
