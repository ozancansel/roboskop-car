import QtQuick 2.0

Rectangle {

    property string text                :   "<button>"
    property real   textFontHeight        :   0.2
    property color  fillColor           :   "#191919"
    property color  checkedColor        :   "#A8A8A8"
    property color  textColor           :   "white"
    property real   radiusPortion       :   0.1
    property color  borderColor         :   "black"
    property real   borderWidthPortion  :   0.01
    property bool   checkable           :   false
    property bool   checked             :   false

    property alias mouseArea            :   area
    property Component  customComponent :   emptyComp
    property alias renderedItem         :   customComponentLoader.item

    Component{
        id  :   emptyComp
        Item{

        }
    }

    border.width    :   height * borderWidthPortion
    color           :   {
        if(checkable && checked)
            return checkedColor

        if(area.pressed)
            return checkedColor
        else
            return fillColor
    }

    border.color    :   borderColor
    radius          :   height * radiusPortion

    Loader{
        property var target :   parent
        id                  :   customComponentLoader
        sourceComponent     :   customComponent
        anchors.centerIn    :   parent
    }

    Text{
        id              :   buttonText
        font.pixelSize  :   parent.height * textFontHeight
        text            :   parent.text
        anchors.centerIn:   parent
        color           :   textColor
        scale           :   checked ? 1.1 : 1
        horizontalAlignment :   Text.AlignHCenter
        verticalAlignment   :   Text.AlignVCenter

        Behavior on scale{
            NumberAnimation{
                duration    :   150
            }
        }
    }

    MouseArea{
        id              :   area
        anchors.fill    :   parent
        onClicked       :   {
            if(!checkable)
                return

            checked = !checked
        }
    }
}
