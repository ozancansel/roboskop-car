import QtQuick 2.0

Rectangle{

    property real   containerRadius     :   0.2
    property bool   empty               :   false
    property real   borderW             :   0.012
    property bool   pressed             :    false
    property color  pressedColor        :   "#222222"

    id              :   control

    color           :   {
        if(empty)
            return "transparent"
        else if(pressed)
            return pressedColor
        else
            return "#191919"
    }
    border.width    :   empty ? Math.min(width , height) * borderW : 0
    radius          :   Math.min(width , height) * containerRadius
}
