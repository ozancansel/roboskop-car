import QtQuick 2.0
import GezginBot 1.0

QtObject {

    property RoboGezginConfig    config

    function calculateThrottle(th){
    }

    function calculateSteering(steering){
    }

    function calculate(throttle , steering){
        return {throttle : calculateThrottle(throttle) , steering : calculateSteering(steering)}
    }

}
