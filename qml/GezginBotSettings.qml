import QtQuick 2.0
import GezginBot 1.0
import QtQuick.Controls 2.1
import "form"

FormPage{

    //Width
    readonly property real labelsWidth                      :   0.95
    readonly property real labelsContainerWidthPortion      :   0.5
    readonly property real settingsControlsInnerWidth       :   0.9
    readonly property real addSensorControlInnerWidth       :   0.94


    //Height
    readonly property real labelsHeight                     :   0.1
    readonly property real settingsControlsInnerHeight      :   0.67
    readonly property real addSensorControlHeight           :   0.6
    readonly property real addSensorScontrolInnerHeight     :   0.8

    //Font
    readonly property real addSensorTemplateFontHeight      :   0.55
    readonly property real headerTextFontSize               :   0.7

    //Right margin
    readonly property real saveButtonRightMargin            :   0.05

    //Top margin
    readonly property real drivingButtonsContainerTopMargin :   0.05

    //Spacing
    readonly property real drivingSettingsSpacing           :   0.017

    //Radius
    readonly property real addSensorControlRadius           :   0.1

    //Color
    readonly property color textColor       :   "white"

    signal sensorAddRequired(var sen)
    property RoboGezginConfig   config

    Rectangle {

        color           :   "#383838"
        anchors.fill    :   parent

        Column{

            id                  :   settingsColumn
            width               :   parent.width * labelsContainerWidthPortion
            height              :   parent.height
            spacing             :   height * drivingSettingsSpacing
            anchors.top         :   parent.top
            anchors.topMargin   :   parent.height * drivingButtonsContainerTopMargin

            Item{

                width                       :   parent.width * labelsWidth
                height                      :   parent.height * labelsHeight
                anchors.horizontalCenter    :   parent.horizontalCenter

                Text {
                    id                  :   headerText
                    text                :   "Sürüş Ayarları"
                    anchors.centerIn    :   parent
                    font.pixelSize      :   parent.height * headerTextFontSize
                    color               :   "white"
                }
            }

            ControlContainer{
                width                       :   parent.width * labelsWidth
                height                      :   parent.height * labelsHeight
                anchors.horizontalCenter    :   parent.horizontalCenter


                LabelWithTextEdit{
                    id                          :   label
                    width                       :   parent.width * settingsControlsInnerWidth
                    height                      :   parent.height *   settingsControlsInnerHeight
                    anchors.centerIn            :   parent
                    textColor                   :   textColor
                    labelText                   :   "Araç İsmi"
                }
            }

            ControlContainer{
                width                       :   parent.width * labelsWidth
                height                      :   parent.height * labelsHeight
                anchors.horizontalCenter    :   parent.horizontalCenter

                SettingsSpinBoxWithLabel{
                    id                          :   sensivityLabel
                    text                        :   "Direksiyon Hassasiyet"
                    value                       :   config.sensivity
                    width                       :   parent.width * settingsControlsInnerWidth
                    height                      :   parent.height * settingsControlsInnerHeight
                    labelAlias.color            :   textColor
                    anchors.centerIn            :   parent
                }
            }

            ControlContainer{
                width                       :   parent.width * labelsWidth
                height                      :   parent.height * labelsHeight
                anchors.horizontalCenter    :   parent.horizontalCenter

                SettingsSpinBoxWithLabel{
                    id                          :   deadzoneLabel
                    text                        :   "Ölü Bölge"
                    width                       :   parent.width *  settingsControlsInnerWidth
                    height                      :   parent.height * settingsControlsInnerHeight
                    value                       :   config.deadzone
                    labelAlias.color            :   textColor
                    anchors.centerIn            :   parent
                }
            }

            ControlContainer{
                width                       :   parent.width * labelsWidth
                height                      :   parent.height * labelsHeight
                anchors.horizontalCenter    :   parent.horizontalCenter

                SettingsSpinBoxWithLabel{
                    id                          :   maxForwardLabel
                    text                        :   "Maksimum İleri Güç"
                    width                       :   parent.width * settingsControlsInnerWidth
                    height                      :   parent.height * settingsControlsInnerHeight
                    value                       :   config.maxForwardThrottle
                    anchors.centerIn            :   parent
                    labelAlias.color            :   textColor
                }
            }

            ControlContainer{
                width                       :   parent.width * labelsWidth
                height                      :   parent.height * labelsHeight
                anchors.horizontalCenter    :   parent.horizontalCenter

                SettingsSpinBoxWithLabel{
                    id                          :   maxRearLabel
                    text                        :   "Maksimum Geri Güç"
                    width                       :   parent.width * settingsControlsInnerWidth
                    height                      :   parent.height * settingsControlsInnerHeight
                    value                       :   config.maxRearThrottle
                    anchors.centerIn            :   parent
                    labelAlias.color            :   textColor
                }
            }


            ControlContainer{
                width                       :   parent.width * labelsWidth
                height                      :   parent.height * labelsHeight
                anchors.horizontalCenter    :   parent.horizontalCenter


                SettingsSpinBoxWithLabel{
                    id                          :   maxSteering
                    text                        :   "Maksimum Dönüş"
                    width                       :   parent.width * settingsControlsInnerWidth
                    height                      :   parent.height *   settingsControlsInnerHeight
                    value                       :   config.maxSteering
                    anchors.centerIn            :   parent
                    labelAlias.color            :   textColor
                }
            }

            ControlContainer{
                width                       :   parent.width * labelsWidth
                height                      :   parent.height * labelsHeight
                anchors.horizontalCenter    :   parent.horizontalCenter


                SettingsSpinBoxWithLabel{
                    id                          :   accelerationMagnitude
                    text                        :   "Hızlanma Şiddeti"
                    width                       :   parent.width * settingsControlsInnerWidth
                    height                      :   parent.height *   settingsControlsInnerHeight
                    value                       :   config.accelerationMagnitude
                    anchors.centerIn            :   parent
                    labelAlias.color            :   textColor
                }
            }
        }

        Column{
            spacing             :   height * drivingSettingsSpacing
            anchors.left        :   settingsColumn.right
            anchors.right       :   parent.right
            anchors.top         :   parent.top
            anchors.topMargin   :   parent.height * drivingButtonsContainerTopMargin
            anchors.bottom      :   parent.bottom

            Item{

                width                       :   parent.width * labelsWidth
                height                      :   parent.height * labelsHeight
                anchors.horizontalCenter    :   parent.horizontalCenter

                Text {
                    id                  :   sensorAddHeader
                    text                :   "Sensör Ekle"
                    anchors.centerIn    :   parent
                    font.pixelSize      :   parent.height * headerTextFontSize
                    color               :   "white"
                }
            }

            ControlContainer{

                width                       :   parent.width  * labelsWidth
                height                      :   parent.height * addSensorControlHeight
                containerRadius             :   addSensorControlRadius
                empty                       :   true

                SensorAddControl{
                    id                          :   addSensorControl
                    anchors.horizontalCenter    :   parent.horizontalCenter
                    anchors.verticalCenter      :   parent.verticalCenter
                    width                       :   parent.width * addSensorControlInnerWidth
                    height                      :   parent.height * addSensorScontrolInnerHeight
                    onAddSensor                 :   {
                        //<sensorPar>
                        sensorAddRequired(sensorPar)
                    }
                    onDisplayMessage            :   displayMessage(message)
                }
            }

            ControlButton{
                width                       :   parent.width * labelsWidth
                height                      :   parent.height * labelsHeight
                text                        :   "Manyetik Alan Sensörü Ekle"
                textFontHeight              :   addSensorTemplateFontHeight
                mouseArea.onClicked         :   sensorAddRequired({ sensorName : "Manyetik Alan" , pin   :   1  ,   period  :   100 , unit : "G"})
                visible                     :   false
            }

            ControlButton{
                width                       :   parent.width * labelsWidth
                height                      :   parent.height * labelsHeight
                text                        :   "Ldr Sensörü Ekle"
                textFontHeight              :   addSensorTemplateFontHeight
                mouseArea.onClicked         :   sensorAddRequired({ sensorName : "Ldr" , pin   :   -1  ,   period  :   200 , unit : "lm"})
                visible                     :   false
            }
        }

        Component.onCompleted: {
            config.sensivity = Qt.binding(function(){ return sensivityLabel.value })
            config.deadzone = Qt.binding(function(){return deadzoneLabel.value})
            config.maxForwardThrottle = Qt.binding(function(){return maxForwardLabel.value})
            config.maxRearThrottle = Qt.binding(function(){return maxRearLabel.value})
            config.maxSteering = Qt.binding(function(){return maxSteering.value})
            config.accelerationMagnitude = Qt.binding(function(){return accelerationMagnitude.value});
        }
    }
}
