import QtQuick 2.0
import "util"

QtObject {
    function    throttle(th , ang){
        var mappedTh =  Math.floor(NumericHelper.map(th , 0 , 100 , 0 , 255));
        var mappedSteering = Math.floor(NumericHelper.map(ang , 0 , 100 , 0 , 255));
        bluetooth.send("{^" + mappedTh + "," + mappedSteering + "}")
    }
}
