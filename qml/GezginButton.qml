import QtQuick 2.7
import QtQuick.Controls 2.1

Button {

    property color  fontColor       :   "white"

    id              :   ctl
    background      :    ControlContainer {
        id          :   ctlBack
        pressed     :   ctl.down || ctl.checked
        border.width:   ctl.checked ? height * 0.05 : 0
        border.color:   "white"
        scale       :   ctl.pressed  ? 1.1  : 1.0
        Text {
            id      :   txt
            text    :   ctl.text
            anchors.centerIn    :   parent
            font.pixelSize      :   parent.height * 0.4
            color               :   fontColor
        }


        Behavior on scale {
            NumberAnimation{
                duration    :   100
            }
        }
    }

    contentItem     :   Item {}
}
