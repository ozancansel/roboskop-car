import QtQuick 2.0
import QtQuick.Controls 2.1

Rectangle {

    readonly property real headerHeight     :   0.1
    readonly property color consoleColor        :       "black"

    color           :   "black"

    function        appendUserName(){
        consoleIn.append("gezginbot@user:   ")
    }

    function        newLine(){
        consoleIn.append("\n")
    }

    function        unknowCommand(str){
        consoleIn.append("Bilinmeyen komut \"" + str +"\"")
    }

    TextArea    {
        id              :   consoleIn
        width           :   parent.width
        height          :   parent.height
        color           :   "green"
        font.bold       :   true
        selectByKeyboard:   false
        selectByMouse   :   false

        cursorDelegate  :   Component{
            Rectangle{
                id      :   cursorDel
                color   :   "white"
                width   :   height * 0.6

                SequentialAnimation{
                    loops       :   Animation.Infinite
                    running     :   true
                    ScriptAction{
                        script  :   cursorDel.color =   "white"
                    }
                    PauseAnimation{
                        duration    :   700
                    }
                    ScriptAction{
                        script  :   cursorDel.color =   "transparent"
                    }

                    PauseAnimation {
                        duration    :   700
                    }
                }
            }
        }

        Component.onCompleted   :   {
            appendUserName()
        }

        Keys.onReturnPressed    :   {
            console.log("return pressed")
            consoleIn.cursorPosition
            appendUserName()
        }
    }

}
