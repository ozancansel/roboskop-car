import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls 2.1
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import QtQuick.Extras.Private 1.0
import GezginBot 1.0
import "control"
import "form"
import "theme"
import "enum"
import "util"
import "dialog"

FormPage{


    //Width
    readonly property real      carInformationsContainerWidth    :   0.3
    readonly property real      carStatusLabelsWidth        :   0.9
    readonly property real      steeringGaugeWidth          :   0.8
    readonly property real      arrowWidth                  :   0.5
    readonly property real      sensorWidth                 :   0.2

    //Height
    readonly property real      consoleContainerHeight      :   0.0
    readonly property real      motorPowerGaugeHeight       :   1
    readonly property real      carStatusLabelsHeight       :   0.1
    readonly property real      carStatusLabelTextHeight    :   0.3
    readonly property real      fuelTextContainerHeight     :   0.25
    readonly property real      fuelTextHeight              :   0.8
    readonly property real      speedGaugeHeight            :   0.8
    readonly property real      drivingModesHeight          :   0.18
    readonly property real      drivingModesHeaderTextContainerHeight   :   0.32
    readonly property real      drivingModesHeaderTextHeight:   0.5
    readonly property real      arrowHeight                 :   0.5
    readonly property real      carPeripheralControlsHeight :   0.12
    readonly property real      actionsHeight               :   0.12

    //Top Margin
    readonly property real      motorPowerGaugeTopMargin    :   0.1
    readonly property real      fuelTextContainerTopMargin  :   0.1
    readonly property real      carInformationColumnTopMargin   :   0.05

    //Radius
    readonly property real      carStatusLabelsRadius       :   0.08
    readonly property real      sensorReadRadius            :   0.2

    //Background
    readonly property color carStatusLabelsColor            :   "#191919"
    readonly property color carStatusLabelsContainerColor   :   "#383838"
    readonly property color carGaugesContainerColor         :   "#424b4d"

    //Spacing
    readonly property real sensorReadsSpacing               :   0.017
    readonly property real actionsSpacing                   :   0.017

    //Animation sliding properties
    readonly property real manuelDirectionControlsSlidingPortion    :   0.2

    //Constants
    readonly property real noConnection                     :   0
    readonly property real connecting                       :   1
    readonly property real carActive                        :   2
    readonly property real carBroken                        :   3
    readonly property real carDisconnected                  :   4

    //Mapping
    readonly property variant   statusToTextMapping         :   ["Bağlantı Yok" , "Baglaniyor" ,"Aktif" ,  "Arıza" , "Baglanti Koptu"]
    readonly property variant   statusToTextColorMapping    :   ["white" , "green" , "green" , "white" , "white"]
    property variant            occupiedAnalogPins          :   []

    //Driving parameters
    readonly property int   crossSteeringVal                :   50
    readonly property int   hybridSteeringVal               :   25
    readonly property int   turnItselfSteeringVal           :   100
    readonly property int   throttleSliderMultiplier        :   100

    property bool  greetingRunning                          :   false
    property bool           reverseDriving                  :   false

    id          :   povMainContainer

    //Logic properties
    property RoboGezginConfig   config
    property int carStatus                                  :   noConnection
    property int carType                                    :   CarStatusEnum.beginnerType
    property int drivingMode                                :   CarStatusEnum.normalMode
    property int currentlyThrottle                          :   0
    property int currentlySteering                          :   0
    property bool carIsDisabled                             :   carIsActiveButton.checked
    property bool gyroEnabled                               :   gyroEnabledButton.checked && gyroEnabledCondition2 && drivingMode === CarStatusEnum.normalMode
    property bool gyroEnabledCondition2                     :   true

    signal      setInteractive(bool val)
    signal      needConnection()

    Timer   {
        id          :   statusMock
        repeat      :   true
        interval    :   2000
        running     :   false
        onTriggered :   {
            var num = Math.floor(Math.random() * 4.9)
            carStatus = num;
        }
    }

    function    setSteeringVisualVal(val){
        steeringBar.value  =   val
    }

    function    throttle(th , steering){
        if(bluetooth.bufferedCommandsLength < 1){
            directThrottle(th , steering)
        }
    }

    function    directThrottle(th , steering){

        //Eger ki araba disable ise geri donuluyor
        if(carIsDisabled)
            return

        currentlyThrottle   =   th;
        currentlySteering   =   steering;
        console.log("Th : " + th + "Steering : " + steering);

        if(reverseDriving){
            th *= -1;
        }

        btController.throttle(th,  currentlySteering)
    }

    //Analog sensör ekler
    function addSensor(inf){

        if(analogPinIsOccupied(inf.pin)){
            displayMessage("Bu pin'e sensör eklenmiş")

            return
        }


        var newSensor = sensorReadComponent.createObject(controlAndSensorsGrid)

        newSensor.width = Qt.binding(function(){ return buttonsSize.calculatedWidth})
        newSensor.height = Qt.binding(function(){ return buttonsSize.calculatedHeight})
        newSensor.unit = inf.unit
        newSensor.header = inf.sensorName
        newSensor.value = 0
        newSensor.color = carStatusLabelsColor
        newSensor.analogIdx = inf.pin
        newSensor.period = inf.period

        occupiedAnalogPins.push(inf.pin)

        displayMessage(newSensor.header + " sensörü eklendi")

    }

    function    analogPinIsOccupied(pin){

        for(var i = 0; i < occupiedAnalogPins.length; i++){
            if(pin === occupiedAnalogPins[i])
                return true
        }

        return false
    }


    Component.onCompleted   : {

        //        addSensor({
        //                      unit  :   "lm",
        //                      sensorName : "Ldr" ,
        //                      pin        : 0 ,
        //                      period        :   200
        //                  }
        //                      )

        //        addSensor({
        //                      unit  :   "lm",
        //                      sensorName : "Ldr" ,
        //                      pin        : 1 ,
        //                      period        :   200
        //                  }
        //                      )

        //        addSensor({
        //                      unit  :   "lm",
        //                      sensorName : "Ldr" ,
        //                      pin        : 2 ,
        //                      period        :   200
        //                  }
        //                      )

        //                addSensor({
        //                              unit  :   "lm",
        //                              sensorName : "Ldr" ,
        //                              pin        : 3 ,
        //                              period        :   200
        //                          }
        //                              )

        //        addSensor({
        //                      unit  :   "lm",
        //                      sensorName : "Ldr" ,
        //                      pin        : 4 ,
        //                      period        :   200
        //                  }
        //                      )

        //        addSensor({
        //                      unit  :   "lm",
        //                      sensorName : "Ldr" ,
        //                      pin        : 5 ,
        //                      period        :   200
        //                  }
        //                      )

    }

    //Farları enable/disable eder
    function setLigtsEnabled(enabled){
        var enStr = enabled ? "1" : "0"
        bluetooth.send("{l" + enStr + "}");
        displayMessage("Farlar " + (enabled ? "açık" : "kapalı"));
    }

    //Arabayı durdurur
    function    stop(){
        directThrottle(0 , 0)
    }

    Component{
        id          :   sensorReadComponent

        SensorSignalTemplate{
            id                          :   template
            mouseArea.onPressAndHold    :   {
                console.log("Destroying sensor")
                template.destroy()
            }
        }
    }

    Toast{
        id              :   toast
        container       :   parent
        fillColor       :   "#191919"
        textColor       :   "white"
    }

    AccelerometerInput{
        id                      :   accelerometer
        config                  :   povMainContainer.config
        onThrottleChanged       :   {
            throttle(th, steering)
        }

        active                  :   gyroEnabled
    }

    states      :   [
        State {
            name                : "notConnected"; when : carStatus === noConnection && !carIsDisabled

            PropertyChanges {
                target          :   carStatusBackground
                color           :   carStatusLabelsColor
            }
        },
        State {
            name                :   "connecting"; when : carStatus === connecting && !carIsDisabled
            PropertyChanges {
                target          :   carStatusBackground
                border.width    :   0
            }
        } ,
        State {
            name                :   "connected"; when : carStatus === carActive && !carIsDisabled
            PropertyChanges {
                target          :   carStatusBackground
                border.width    :   carStatusBackground.height * 0.02
                border.color    :   "green"
                color           :   carStatusLabelsColor
            }
        },
        State {
            name                :   "active"; when : carStatus === carActive && !carIsDisabled
        } ,
        State {
            name                :   "broken"; when : carStatus === carBroken
            PropertyChanges {
                target      :   carStatusBackground
                border.width:   0
            }
        } ,
        State {
            name                :   "disconnected"; when : carStatus === carDisconnected
            PropertyChanges {
                target      :   carStatusBackground
                border.width:   0
            }
        }
    ]

    transitions :   [
        Transition {
            to          :   "notConnected"


            SequentialAnimation{
                id          :   notConnectedAnimation
                loops       :   Animation.Infinite
                ScriptAction{
                    script :   carStatusBackground.border.width = carStatusBackground.height * 0.1
                }

                ColorAnimation {
                    target      :   carStatusBackground
                    property    :   "border.color"
                    from        :   carStatusLabelsContainerColor
                    to          :   carStatusLabelsColor
                    duration    :   400
                }

                PauseAnimation {
                    duration    :   100
                }

                ColorAnimation {
                    target      :   carStatusBackground
                    property    :   "border.color"
                    from        :   carStatusLabelsColor
                    to          :   carStatusLabelsContainerColor
                    duration    :   400
                }
            }
        } ,
        Transition {
            to          :   "connecting"
            SequentialAnimation{
                id          :   connectingAnimation
                loops       :   Animation.Infinite

                ColorAnimation {
                    target      :   carStatusBackground
                    from        :   carStatusLabelsColor
                    to          :   "white"
                    duration    :   400
                    property    :   "color"
                }

                PauseAnimation {
                    duration    :   150
                }

                ColorAnimation {
                    target          :   carStatusBackground
                    from            : "white"
                    to              : carStatusLabelsColor
                    duration        : 400
                    property    :   "color"
                }
            }
        },
        Transition {
            to      :   "broken"

            SequentialAnimation{
                id          :   brokenAnimation
                loops       :   Animation.Infinite
                onStopped   :   {
                    carStatusBackground.color = carStatusLabelsColor
                }

                ColorAnimation {
                    target  :   carStatusBackground
                    property:   "color"
                    from    : "red"
                    to      : "#e50000"
                    duration: 300
                }
                PauseAnimation {
                    duration: 100
                }
                ColorAnimation {
                    target  :   carStatusBackground
                    property:   "color"
                    from    :   "#e50000"
                    to      :   "red"
                    duration:   400
                }
            }
        } ,

        Transition {
            to      :   "disconnected"

            SequentialAnimation{
                id          :   disconnectedAnimation
                loops       :   Animation.Infinite
                onStopped   :   {
                    console.log("on stopped")
                    carStatusBackground.color = carStatusLabelsColor
                }

                ColorAnimation {
                    target  :   carStatusBackground
                    property:   "color"
                    from    : "red"
                    to      : "#e50000"
                    duration: 300
                }
                PauseAnimation {
                    duration: 100
                }
                ColorAnimation {
                    target  :   carStatusBackground
                    property:   "color"
                    from    :   "#e50000"
                    to      :   "red"
                    duration:   400
                }
            }
        }
    ]

    GezginBtController{
        id              :   btController
    }

    onDrivingModeChanged    :   {
        if(drivingMode === CarStatusEnum.normalMode)
            manuelControlsButton.checked = false
        if(drivingMode)
            gyroEnabled.checked = false
    }

    onGyroEnabledChanged    :   {
        if(gyroEnabled)
            toast.displayMessage("Gyro aktif")
        else
            toast.displayMessage("Gyro kapalı")

        stop()
    }

    onReverseDrivingChanged :   {
        if(reverseDriving)
            toast.displayMessage("Ters Sürüş Aktif")
        else
            toast.displayMessage("Ters Sürüş Kapalı")

        stop()
    }


    ResponsiveContainer {
        id              :   responsiveContainer
        anchors.fill    :   parent
    }

    Rectangle {
        id              :   povContainer
        anchors.fill    :   parent

        Rectangle   {

            id                  :   carInformationsSection
            width               :   parent.width * carInformationsContainerWidth
            anchors.top         :   parent.top
            anchors.left        :   parent.left
            anchors.bottom      :   parent.bottom
            height              :   mainColumn.height
            color               :   carStatusLabelsContainerColor

            Rectangle   {
                anchors.top     :   parent.top
                anchors.bottom  :   parent.bottom
                anchors.right   :   parent.right
                width           :   2
                color           :   "white"
            }

            Column  {
                id                          :   mainColumn
                anchors.top                 :   parent.top
                anchors.topMargin           :   parent.width * carInformationColumnTopMargin
                anchors.bottom              :   parent.bottom
                width                       :   parent.width * carStatusLabelsWidth
                anchors.horizontalCenter    :   parent.horizontalCenter
                spacing                     :   responsiveContainer.getScaledY(12)

                Item {
                    id          :   roboskopTextContainer
                    width       :   parent.width
                    height      :   responsiveContainer.getScaledY(100)

                    Text {
                        id                  :   roboskopText
                        text                :   qsTr("Roboskop")
                        anchors.centerIn    :   parent
                        color               :   "White"
                        font.pixelSize      :   parent.height * 0.55
                    }
                }

                Item   {
                    id          :   carTypeContainer
                    width       :   parent.width
                    height      :   responsiveContainer.getScaledY(60)

                    Rectangle{
                        id              :   carTypeBackground
                        radius          :   height * carStatusLabelsRadius
                        anchors.fill    :   parent
                        color           :   carStatusLabelsColor
                    }

                    Text {
                        id                  :   carTypeText
                        text                :   carStatus === carActive ? "RoboGezgin" : "Tip Bilinmiyor"
                        font.pixelSize      :   parent.height * 0.4
                        color               :   "white"
                        anchors.centerIn    :   parent
                    }
                }

                Item{
                    id          :   carStatusContainer
                    width       :   parent.width
                    height      :   responsiveContainer.getScaledY(60)

                    MouseArea{
                        anchors.fill    :   parent
                        onClicked       :   {
                            if(carStatus === carDisconnected || carStatus === noConnection){
                                //Bluetooth sayfasina yonlendiriliyor
                                needConnection()
                            }
                        }
                        onPressAndHold  :   {
                            if(carStatus === carActive){
                                bluetooth.closeSocket()
                                displayMessage("Baglanti kesildi.")
                            }
                        }
                    }

                    Rectangle{
                        id              :   carStatusBackground
                        anchors.fill    :   parent
                        radius          :   height * carStatusLabelsRadius
                        color           :   carStatusLabelsColor
                    }

                    Text {
                        id                  :   statusText
                        text                :   {
                            if(carIsDisabled)
                                return "Pasif"
                            return statusToTextMapping[carStatus]
                        }
                        font.pixelSize      :   parent.height * 0.4
                        color               :   statusToTextColorMapping[carStatus]
                        anchors.centerIn    :   parent
                    }
                }
            }

            Column{
                width                       :   parent.width * 0.9
                anchors.bottom              :   parent.bottom
                anchors.horizontalCenter    :   parent.horizontalCenter
                anchors.bottomMargin        :   width * 0.1

                VirtualJoystick {
                    id                          :   joystick
                    width                       :   parent.width
                    height                      :   width
                    anchors.margins             :   parent.width * 0.1
                    onJoystickXChanged          :   throttle( calculateMagnitude() * NumericHelper.sign(joystickY) * 100 , joystickY > 0 ? Math.floor(joystickX * 100) : -Math.floor(joystickX * 100))
                    onJoystickYChanged          :   throttle(calculateMagnitude() * NumericHelper.sign(joystickY) * 100 , joystickY > 0 ? Math.floor(joystickX * 100) : -Math.floor(joystickX * 100))
                    onActiveChanged             :   {
                        if(active) {
                            lockScreenButton.checked = true
                        }
                        directThrottle(0 , 0);
                    }
                }
            }
        }


        Rectangle{
            id                  :   gaugeContainer
            anchors.left        :   carInformationsSection.right
            anchors.right       :   parent.right
            anchors.bottom      :   manuelControlsContainer.top
            anchors.top         :   parent.top
            color               :   "#383838"


            Item {
                id                  :   gaugeMarginedCont
                anchors.fill        :   parent
                anchors.margins     :   10
                anchors.topMargin   :   20

                ControlSizeCalculator{
                    id              :   buttonsSize
                    widthPortion    :   0.27
                    ratio           :   16 / 9
                    spacingRatio    :   1 / 15
                }

                Flickable{

                    id                          :   sensorAndControls
                    anchors.top                 :   parent.top
                    width                       :   parent.width
                    height                      :   parent.height * 0.54
                    contentHeight               :   controlAndSensorsGrid.childrenRect.height
                    contentWidth                :   width
                    anchors.horizontalCenter    :   parent.horizontalCenter
                    clip                        :   true

                    Grid    {
                        id                          :   controlAndSensorsGrid
                        spacing                     :   buttonsSize.spacingWidth
                        columns                     :   3
                        y                           :   buttonsSize.calculatedHeight * 0.2
                        anchors.horizontalCenter    :   parent.horizontalCenter

                        add: Transition {
                            NumberAnimation { properties: "x,y"; easing.type: Easing.OutBounce }
                        }

                        GezginButton{
                            id              :   carIsActiveButton
                            width           :   buttonsSize.calculatedWidth
                            height          :   buttonsSize.calculatedHeight
                            text            :   checked ? "Pasif" : "Aktif"
                            checkable       :   true
                            onCheckedChanged: {
                                if(checked)
                                    directThrottle(0 , 0)
                            }
                        }

                        GezginButton{
                            id              :   gyroEnabledButton
                            width           :   buttonsSize.calculatedWidth
                            height          :   buttonsSize.calculatedHeight
                            text            :   "Gyro"
                            checkable       :   true
                            onCheckedChanged:   {
                                if(checked){
                                    drivingMode =   CarStatusEnum.normalMode
                                    manuelControlsButton.checked = false
                                }
                            }
                        }

                        GezginButton{
                            id              :   manuelControlsButton
                            width           :   buttonsSize.calculatedWidth
                            height          :   buttonsSize.calculatedHeight
                            text            :   "Manuel"
                            checkable       :   true
                            onCheckedChanged:   {
                                if(checked){
                                    drivingMode = CarStatusEnum.manuelMode
                                    gyroEnabledButton.checked = false
                                }
                                else{
                                    drivingMode = CarStatusEnum.normalMode
                                }
                            }
                        }

                        GezginButton{
                            id              :   helloButton
                            width           :   buttonsSize.calculatedWidth
                            height          :   buttonsSize.calculatedHeight
                            text            :   "Merhaba"
                            onClicked       :   {

                                if(greetingRunning) return

                                gezginGreetingReaction.start()
                            }
                        }
                        GezginButton{
                            id              :   ummButton
                            width           :   buttonsSize.calculatedWidth
                            height          :   buttonsSize.calculatedHeight
                            text            :   "Test"
                            onClicked       :   {
                                if(greetingRunning) return

                                gezginUmmReaction.start()
                            }
                        }

                        GezginButton{
                            id              :   lockScreenButton
                            width           :   buttonsSize.calculatedWidth
                            height          :   buttonsSize.calculatedHeight
                            text            :   "Kilitle"
                            checkable       :   true
                            onCheckedChanged:   {
                                if(checked)
                                    setInteractive(false)
                                else
                                    setInteractive(true)
                            }
                        }
                    }
                }

                Column  {
                    id          :   sensorBarColumn
                    width       :   parent.width
                    height      :   parent.height * 0.4
                    anchors.bottom  :   parent.bottom
                    spacing     :   height * 0.1

                    SensorBar{
                        id          :   throttleBar
                        width       :   parent.width
                        height      :   parent.height * 0.4
                        value       :   currentlyThrottle
                        backgroundComp  :
                            Rectangle{
                            color   :   "#3e3e3e"
                            height  :   target.height
                            Text {
                                id                  :   throttleBackgroundText
                                text                :   "Güç 0%"
                                anchors.centerIn    :   parent
                                font.pixelSize      :   parent.height * 0.35
                                color               :   "white"
                                opacity             :   throttleBar.value === throttleBar.midVal ? 1 : 0
                                Behavior on opacity {
                                    NumberAnimation {
                                        duration    :   100
                                    }
                                }
                            }

                            Item {
                                id              :   throttleLockImgContainer
                                height          :   parent.height
                                width           :   height
                                anchors.right   :   parent.right
                                anchors.rightMargin :   height * 0.5
                                opacity         :   carIsDisabled ? 1 : 0

                                Image {
                                    id                  :   throttleLockImg
                                    source              :   "/res/img/lock.png"
                                    height              :   parent.height * 0.55
                                    anchors.centerIn    :   parent
                                    fillMode            :   Image.PreserveAspectFit
                                }

                                Behavior on opacity {
                                    NumberAnimation{
                                        duration    :   250
                                    }
                                }
                            }
                        }

                        valueBarComp    :   Rectangle{
                            color   :   "#1bc2ff"
                            height  :   target.height

                            Text {
                                id                  :   throttleBarValueText
                                text                :   throttleBar.value + "%"
                                anchors.centerIn    :   parent
                                font.pixelSize      :   parent.height * 0.3
                                opacity             :   parent.width > width ? 1 : 0
                                color               :   "white"

                                Behavior on opacity {
                                    NumberAnimation {
                                        duration    :   100
                                    }
                                }
                            }
                        }
                    }

                    SensorBar{
                        id          :   steeringBar
                        width       :   parent.width
                        height      :   parent.height * 0.4
                        value       :   currentlySteering
                        backgroundComp  :
                            Rectangle{
                            color   :   "#3e3e3e"
                            height  :   target.height

                            Text {
                                id              :   steeringBarHeaderText
                                text            :   "Donus 0%"
                                font.pixelSize  :   parent.height * 0.35
                                color           :   "white"
                                anchors.centerIn:   parent
                                opacity         :   steeringBar.value === steeringBar.midVal ? 1 : 0
                            }

                            Item {
                                id                  :   steeringLockImgContainer
                                height              :   parent.height
                                width               :   height
                                anchors.right       :   parent.right
                                anchors.rightMargin :   height * 0.5
                                opacity             :   carIsDisabled ? 1 : 0

                                Image {
                                    id                  :   steeringLockImg
                                    source              :   "/res/img/lock.png"
                                    height              :   parent.height * 0.55
                                    anchors.centerIn    :   parent
                                    fillMode            :   Image.PreserveAspectFit
                                }

                                Behavior on opacity {
                                    NumberAnimation{
                                        duration    :   250
                                    }
                                }
                            }
                        }
                        valueBarComp    :   Rectangle{

                            color   :   "#1bc2ff"
                            height  :   target.height

                            Text {
                                id                  :   steeringBarValueText
                                text                :   steeringBar.value + "%"
                                anchors.centerIn    :   parent
                                font.pixelSize      :   parent.height * 0.3
                                color               :   "white"
                                opacity             :   parent.width > width ? 1 : 0

                                Behavior on opacity {
                                    NumberAnimation{
                                        duration    :   100
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        Rectangle{
            id                  :   manuelControlsContainer

            anchors.left        :   carInformationsSection.right
            anchors.right       :   parent.right
            anchors.bottom      :   parent.bottom
            height              :   drivingMode == CarStatusEnum.manuelMode ?  parent.height * 0.5 : 0

            Behavior on height {
                SmoothedAnimation{
                    velocity    :   height * 0.65
                }

                ScriptAction{
                    script  :   {
                        manuelControlsContainer.visible =   drivingMode === CarStatusEnum.manuelMode
                    }
                }
            }

            color               :   carStatusLabelsContainerColor

            Rectangle{
                anchors.left    :   parent.left
                anchors.right   :   parent.right
                height          :   2
            }

            Item{
                id              :   controlsContainer
                width           :   parent.width * 0.53
                height          :   parent.height * 0.9
                anchors.centerIn:   parent

                Grid    {

                    id                  :   controlGrid
                    columns             :   3
                    rows                :   3
                    anchors.fill        :   parent
                    spacing             :   width * 0.01

                    ControlButton   {
                        id          :   forwardLeftButton
                        width       :   parent.width * 0.3
                        text        :   ""
                        customComponent :   Component{
                            Arrow{
                                width   :   target.width * arrowWidth
                                height  :   target.height * arrowHeight
                                rotation    :   -45
                                x       :   forwardLeftButton.mouseArea.pressed ? width * -manuelDirectionControlsSlidingPortion : 0
                                y       :   forwardLeftButton.mouseArea.pressed ? height * -manuelDirectionControlsSlidingPortion : 0

                                Behavior on x{
                                    SmoothedAnimation{

                                    }
                                }
                                Behavior on y {
                                    SmoothedAnimation{

                                    }
                                }
                            }
                        }

                        height      :   parent.height * 0.3
                        mouseArea.onPressed :   throttle(1 * throttleSliderMultiplier , -hybridSteeringVal)
                        mouseArea.onReleased:   stop()
                    }

                    ControlButton   {
                        id          :   forwardButton
                        width       :   parent.width * 0.3
                        height      :   parent.height * 0.3
                        text        :   ""


                        customComponent :   Component{
                            Arrow{
                                width   :   target.width * arrowWidth
                                height  :   target.height * arrowHeight
                                y       :   forwardButton.mouseArea.pressed ? height * -manuelDirectionControlsSlidingPortion : 0

                                Behavior on x{
                                    SmoothedAnimation{

                                    }
                                }

                                Behavior on y {
                                    SmoothedAnimation{

                                    }
                                }
                            }
                        }
                        mouseArea.onPressed :   throttle(1 * throttleSliderMultiplier , 0)
                        mouseArea.onReleased:   stop()
                    }

                    ControlButton   {
                        id          :   forwardRightButton
                        width       :   parent.width * 0.3
                        text        :   ""
                        height      :   parent.height * 0.3

                        customComponent :   Component{
                            Arrow{
                                width   :   target.width * arrowWidth
                                height  :   target.height * arrowHeight
                                rotation:   45
                                x       :   forwardRightButton.mouseArea.pressed ? width * manuelDirectionControlsSlidingPortion : 0
                                y       :   forwardRightButton.mouseArea.pressed ? height * -manuelDirectionControlsSlidingPortion : 0

                                Behavior on x{
                                    SmoothedAnimation{

                                    }
                                }

                                Behavior on y {
                                    SmoothedAnimation{

                                    }
                                }
                            }
                        }

                        mouseArea.onPressed :   throttle(1 * throttleSliderMultiplier  , hybridSteeringVal)
                        mouseArea.onReleased:   stop()
                    }

                    ControlButton   {
                        id          :   leftButton
                        width       :   parent.width * 0.3
                        text        :   ""
                        height      :   parent.height * 0.3

                        customComponent :   Component{
                            Arrow{
                                width   :   target.width * arrowWidth
                                height  :   target.height * arrowHeight
                                rotation:   -90
                                x       :   leftButton.mouseArea.pressed ? width * -manuelDirectionControlsSlidingPortion : 0

                                Behavior on x{
                                    SmoothedAnimation{

                                    }
                                }

                                Behavior on y {
                                    SmoothedAnimation{

                                    }
                                }
                            }
                        }

                        mouseArea.onPressed :   throttle(1 * throttleSliderMultiplier , -crossSteeringVal)
                        mouseArea.onReleased:   stop()
                    }


                    Item   {
                        id          :   rotateItself
                        width       :   parent.width * 0.3
                        height      :   parent.height * 0.3
                    }


                    ControlButton   {
                        id          :   rightButton
                        width       :   parent.width * 0.3
                        text        :   ""
                        height      :   parent.height * 0.3
                        customComponent :   Component{
                            Arrow{
                                width       :   target.width * arrowWidth
                                height      :   target.height * arrowHeight
                                rotation:   90
                                x       :   rightButton.mouseArea.pressed ? width * manuelDirectionControlsSlidingPortion : 0

                                Behavior on x{
                                    SmoothedAnimation{

                                    }
                                }

                                Behavior on y {
                                    SmoothedAnimation{

                                    }
                                }
                            }
                        }

                        mouseArea.onPressed :   throttle(1   * throttleSliderMultiplier  , crossSteeringVal)
                        mouseArea.onReleased:   stop()
                    }


                    ControlButton   {
                        id          :   rearLeftButton
                        width       :   parent.width * 0.3
                        text        :   ""
                        height      :   parent.height * 0.3

                        customComponent :   Component{
                            Arrow{
                                width       :   target.width * arrowWidth
                                height      :   target.height * arrowHeight
                                rotation    :   235
                                x           :   rearLeftButton.mouseArea.pressed ? width * -manuelDirectionControlsSlidingPortion : 0
                                y           :   rearLeftButton.mouseArea.pressed ? height * manuelDirectionControlsSlidingPortion : 0

                                Behavior on x{
                                    SmoothedAnimation{

                                    }
                                }

                                Behavior on y {
                                    SmoothedAnimation{

                                    }
                                }
                            }
                        }
                        mouseArea.onPressed :   throttle(-1 * throttleSliderMultiplier , -hybridSteeringVal)
                        mouseArea.onReleased:   stop()
                    }

                    ControlButton   {
                        id          :   rearButton
                        width       :   parent.width * 0.3
                        text        :   ""
                        height      :   parent.height * 0.3

                        customComponent :   Component{
                            Arrow{
                                width       :   target.width * arrowWidth
                                height      :   target.height * arrowHeight
                                rotation    :   180
                                y           :   rearButton.mouseArea.pressed ? height * manuelDirectionControlsSlidingPortion : 0

                                Behavior on x   {
                                    SmoothedAnimation{

                                    }
                                }

                                Behavior on y {
                                    SmoothedAnimation{

                                    }
                                }
                            }
                        }
                        mouseArea.onPressed :   throttle(-1 * throttleSliderMultiplier , 0)
                        mouseArea.onReleased:   stop()
                    }

                    ControlButton   {
                        id          :   realRightButton
                        width       :   parent.width * 0.3
                        text        :   ""
                        height      :   parent.height * 0.3
                        customComponent :   Component{
                            Arrow{
                                width       :   target.width * arrowWidth
                                height      :   target.height * arrowHeight
                                rotation    :   135
                                x           :   realRightButton.mouseArea.pressed ? width * manuelDirectionControlsSlidingPortion : 0
                                y           :   realRightButton.mouseArea.pressed ? height * manuelDirectionControlsSlidingPortion : 0

                                Behavior on x{
                                    SmoothedAnimation{

                                    }
                                }

                                Behavior on y {
                                    SmoothedAnimation{

                                    }
                                }
                            }
                        }

                        mouseArea.onPressed :   throttle(-1 * throttleSliderMultiplier , hybridSteeringVal)
                        mouseArea.onReleased:   stop()
                    }
                }
            }

            Item{
                id                  :   rotatingItselfButtonsContainer
                anchors.left        :   controlsContainer.right
                anchors.right       :   parent.right
                anchors.bottom      :   controlsContainer.bottom
                anchors.top         :   controlsContainer.top

                Column{
                    readonly property real  portion     :   SizeBinding.sizePortion(3 , 0.05)
                    id              :   rotatingButtons
                    width           :   parent.width * 0.7
                    height          :   parent.height
                    spacing         :   parent.height * 0.05

                    ControlButton   {
                        id          :   rotateCwButton
                        width       :   parent.width
                        text        :   ""
                        height      :   parent.height * rotatingButtons.portion

                        mouseArea.onPressed :   {
                            throttle(1 * throttleSliderMultiplier , 100)
                        }

                        customComponent :   Component{
                            Image{
                                id          :   rotateIcon
                                width       :   target.width * arrowWidth
                                height      :   target.height * arrowHeight
                                fillMode    :   Image.PreserveAspectFit
                                source      :   "/res/img/rotate-white-128x128.png"


                                RotationAnimation{
                                    target  :   rotateIcon
                                    from    :   0
                                    to      :   360
                                    loops   :   Animation.Infinite
                                    running :   rotateCwButton.mouseArea.pressed
                                }
                            }
                        }
                    }

                    ControlButton   {
                        id          :   rotateCcwButton
                        width       :   parent.width
                        text        :   ""
                        height      :   parent.height * rotatingButtons.portion

                        mouseArea.onPressed :   {
                            throttle(1 * throttleSliderMultiplier , -100)
                        }

                        customComponent :   Component{
                            Image   {
                                id          :   rotateIcon
                                width       :   target.width * arrowWidth
                                height      :   target.height * arrowHeight
                                fillMode    :   Image.PreserveAspectFit
                                source      :   "/res/img/rotate-ccw-white-128x128.png"


                                RotationAnimation{
                                    target  :   rotateIcon
                                    from    :   0
                                    to      :   -360
                                    loops   :   Animation.Infinite
                                    running :   rotateCcwButton.mouseArea.pressed
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    SequentialAnimation{
        id          :   gezginUmmReaction
        ScriptAction{
            script  :   {
                gyroEnabledCondition2   =   false
                greetingRunning         =   true
                //Gyro iptal ediliyor
                directThrottle(50 , 100)
            }
        }


        PauseAnimation {
            duration    :   500
        }

        ScriptAction{
            script  :   {
                directThrottle(50 , -100)
            }
        }

        PauseAnimation {
            duration    :   500
        }

        ScriptAction{
            script  :   {
                directThrottle(0 , 0)
                //Gyro eski haline donuyor
                gyroEnabledCondition2   = true
                greetingRunning         =   false
            }
        }
    }

    Connections {
        target          :   bluetooth
        onStateChanged  :   {
            //Disconnected, None veya Error ise
            if(bluetooth.state === 5 && bluetooth.state === 4){
                carStatus = noConnection
            }
            if(bluetooth.state === 0){
                carStatus = carDisconnected
            }
            if(bluetooth.state === 1)
                carStatus = connecting
            //Eger bagli ise
            if(bluetooth.state === 2)
                carStatus = carActive
        }
    }

    SequentialAnimation {
        id          :   gezginGreetingReaction
        ScriptAction{
            script  :   {
                gyroEnabledCondition2   = false
                greetingRunning         =   true
                directThrottle(100 , 100)
            }
        }
        PauseAnimation  {
            duration    :   1000
        }
        ScriptAction    {
            script  :   {
                directThrottle(0 , 0)
                gyroEnabledCondition2   =   true
                greetingRunning         =   false
            }
        }
    }
}
