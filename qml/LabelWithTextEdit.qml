import QtQuick 2.0
import QtQuick.Controls 2.1


Item {

    //Width
    property real   textWidthPortion        :   0.5

    //Height
    property real   textEditHeight          :   0.9

    //Font
    property real   textFontSize            :   0.45
    property real   textEditFont            :   0.42

    //Radius
    property real   textEditRadius          :   0.1

    //Alias
    property alias      textEditControl     :   textEdit
    property alias      textLabel           :   label

    property color      textColor           :   "white"
    property string     labelText           :   "belirtilmedi"
    property string     placeholderText     :   "Birim Giriniz"
    property string     text                :   textEdit.text


    id      :   control

    Item{
        id              :   textContainer
        anchors.left    :   parent.left
        anchors.top     :   parent.top
        anchors.bottom  :   parent.bottom
        width           :   parent.width * textWidthPortion

        Text    {
            id                      :   label
            text                    :   labelText
            font.pixelSize          :   parent.height * textFontSize
            anchors.verticalCenter  :   parent.verticalCenter
            color                   :   textColor
        }
    }

    TextField {
        id              :   textEdit
        height          :   parent.height * textEditHeight
        anchors.left    :   textContainer.right
        anchors.right   :   parent.right
        placeholderText :   control.placeholderText
        font.pixelSize  :   height * textEditFont


        background          :   Rectangle{
            implicitWidth   :   textEdit.width
            implicitHeight  :   textEdit.height
            radius          :   implicitHeight * textEditRadius
            border.width    :   2

            color           :   textEdit.enabled ? "white" : "#FFA500"
            border.color    :   textEdit.enabled ? "#FFA500" : "transparent"
        }
    }
}
