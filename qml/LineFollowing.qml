import QtQuick 2.0
import "form"

FormPage{

    //enum
    readonly property int       startToFollowCode                   :   0
    readonly property int       calibrateCode                       :   1
    readonly property int       stopToFollowCode                    :   2
    readonly property int       setMaxSpeedCode                     :   4
    readonly property int       setMaxTurningCode                   :   5



    //Color
    readonly property color     controlsTextColor                   :   "white"
    readonly property color     headerTextColor                     :   "white"

    //Height
    readonly property real      settingsControlHeight               :   0.3
    readonly property real      headerTextContainerHeight           :   0.2
    readonly property real      headerTextFontSize                  :   0.4
    readonly property real      settingsColumnHeight                :   0.39

    //Spacing
    readonly property real      controlsSpacing                     :   0.1
    readonly property real      controlButtonsSpacing               :   0.04

    //Width
    readonly property real      settingsControlWidth                :   0.3


    function        calculateWidthPortion(){
        return (1 - (3 * controlButtonsSpacing)) / 4
    }

    function        calibrate(){
        bluetooth.send("{c0," + calibrateCode + "}")
    }

    function        stopToFollow(){
        bluetooth.send("{c0," + stopToFollowCode + "}")
    }

    function        startToFollow(){
        bluetooth.send("{c0," + startToFollowCode + "}")
    }

    function        setMaxSpeed(val){
        bluetooth.send("{c0," + setMaxSpeedCode + "," + val + "}")
    }

    function        setMaxTurningSpeed(val){
        bluetooth.send("{c0," + setMaxTurningCode + "," + val + "}")
    }

    function        enableModule(enabled){
        bluetooth.send("{a0," + (enabled ? 1 : 0) + "}")
    }

    Rectangle{

        color               :   "#383838"
        anchors.fill        :   parent

        Rectangle   {
            width               :   parent.width * 0.7
            height              :   parent.height * 0.6
            anchors.centerIn    :   parent
            border.width        :   3
            color               :   "transparent"

            Item{
                id              :   headerTextContainer
                height          :   parent.height * headerTextContainerHeight
                width           :   parent.width
                Text {
                    id                  :   headerText
                    text                :   qsTr("Çizgi İzleme Kontrol")
                    color               :   headerTextColor
                    font.pixelSize      :   parent.height * headerTextFontSize
                    anchors.centerIn    :   parent
                }
            }

            Row{
                id              :   settingsColumn
                width               :   parent.width * 0.6
                anchors.horizontalCenter    :   parent.horizontalCenter
                height          :   parent.height *   settingsColumnHeight
                anchors.top     :   headerTextContainer.bottom

                spacing         :   height *  controlsSpacing

                SettingsSpinBoxWithLabel{
                    id                      :   maxSpeed
                    text                    :   "Maks. Hız"
                    width                   :   parent.width * 0.5
                    height                  :   parent.height * settingsControlHeight
                    labelAlias.color        :   controlsTextColor
                    anchors.verticalCenter  :   parent.verticalCenter
                    maxVal                  :   200
                    minVal                  :   50
                    value                   :   150
                    spinboxAlias.onValueChanged :   setMaxSpeed(value)
                }

                SettingsSpinBoxWithLabel{
                    id                      :   maxTurning
                    text                    :   "Maks. Dönüş"
                    width                   :   parent.width* 0.5
                    height                  :   parent.height * settingsControlHeight
                    labelAlias.color        :   controlsTextColor
                    anchors.verticalCenter  :   parent.verticalCenter
                    maxVal                  :   255
                    minVal                  :   100
                    value                   :   180
                    spinboxAlias.onValueChanged :   setMaxTurningSpeed(value)
                }
            }

            Row{
                width                       :   parent.width * 0.9
                anchors.horizontalCenter    :   parent.horizontalCenter
                height                      :   parent.height * settingsColumnHeight
                anchors.top                 :   settingsColumn.bottom
                anchors.bottom              :   parent.bottom
                anchors.bottomMargin        :   parent.height * 0.05
                spacing                     :   width *  0.049

                ControlButton{
                    width               :   parent.width * calculateWidthPortion()
                    height              :   parent.height
                    text                :   "Kalibre Et"
                    mouseArea.onClicked :   calibrate()
                }

                ControlButton{
                    width               :   parent.width* calculateWidthPortion()
                    height              :   parent.height
                    text                :   "Başlat"
                    mouseArea.onClicked :   startToFollow()
                }

                ControlButton{
                    width               :   parent.width* calculateWidthPortion()
                    height              :   parent.height
                    text                :   "Durdur"
                    mouseArea.onClicked :   stopToFollow()
                }

                ControlButton{
                    width               :   parent.width * calculateWidthPortion()
                    height              :   parent.height
                    text                :   "Aktive Et"
                    checkable           :   true
                    mouseArea.onClicked :   enableModule(checked)
                }
            }
        }
    }

    Connections {
        target              :   commandInterpreter
        onCommandIncome     :   {
            console.log("Code : " + code +  " Args :  "+ commandArgs)
            if(code === 67){
                displayMessage(commandArgs[0])
            }
        }
    }
}
