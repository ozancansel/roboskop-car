import QtQuick 2.0
import QtQuick.Controls 2.1

Column  {

    //Width
    readonly property real  descLabelsWidth         :   0.4
    readonly property real  addButtonWidth          :   0.4

    //Height
    readonly property real  controlsHeight          :   0.165
    readonly property real  sensorNameTextHeight    :   0.4
    readonly property real  headerTextHeight        :   1 - (controlsHeight * 4) - (positionerSpacing * 3)
    readonly property real  headerTextFontSize      :   0.8
    readonly property real  textInputControlHeight  :   0.9

    //Spacing
    readonly property real positionerSpacing        :   0.05

    //Radius
    readonly property real  textInputRadius         :   0.1

    //Font
    readonly property real  textFontSize            :   0.42

    //Color
    readonly property color labelTextColor          :   "white"

    readonly property string sensorName         :   sensorNameTextControl.text
    readonly property string analogPin          :   analogIdxControl.value
    readonly property string period             :   periodControl.value


    signal      addSensor(var sensorPar)
    signal      displayMessage(string message)

    spacing     :   height * positionerSpacing

    Item    {

        width       :   parent.width
        height      :   parent.height * controlsHeight

        Item {

            id      :   sensorNameLabelContainer
            width   :   parent.width * descLabelsWidth
            height  :   parent.height

            Text    {
                font.pixelSize          :   parent.height * textFontSize
                text                    :   "Sensör İsim"
                color                   :   "white"
                anchors.verticalCenter  :   parent.verticalCenter
            }
        }

        TextField{
            id                      :   sensorNameTextControl
            height                  :   parent.height * textInputControlHeight
            anchors.left            :   sensorNameLabelContainer.right
            anchors.right           :   parent.right
            anchors.verticalCenter  :   parent.verticalCenter
            placeholderText         :   "Sensör İsmini Giriniz"
            font.pixelSize          :   analogIdxControl.labelAlias.font.pixelSize

            background          :   Rectangle{
                implicitWidth   :   sensorNameTextControl.width
                implicitHeight  :   sensorNameTextControl.height
                radius          :   implicitHeight * textInputRadius
                border.width    :   2

                color           :   sensorNameTextControl.enabled ? "white" : "#FFA500"
                border.color    :   sensorNameTextControl.enabled ? "#FFA500" : "transparent"
            }
        }
    }

    LabelWithTextEdit{        id                  :   unitControl
        width               :   parent.width
        height              :   parent.height * controlsHeight
        textColor           :   "white"
        textWidthPortion    :   descLabelsWidth
        labelText           :   "Birim"
        placeholderText     :   "Birim Giriniz"
    }

    SettingsSpinBoxWithLabel    {
        id                  :   analogIdxControl
        text                :   "Analog Pin"
        width               :   parent.width
        height              :   parent.height * controlsHeight
        maxVal              :   5
        minVal              :   0
        labelAlias.color    :   "white"
        textWidthPortion    :   descLabelsWidth
    }

    SettingsSpinBoxWithLabel{
        id                      :   periodControl
        text                    :   "Periyod"
        width                   :   parent.width
        height                  :   parent.height * controlsHeight
        minVal                  :   50
        maxVal                  :   5000
        value                   :   200
        spinboxAlias.stepSize   :   20
        labelAlias.color        :   labelTextColor
        textWidthPortion        :   descLabelsWidth
    }

    ControlButton{
        id                  :   addButton
        width               :   parent.width * addButtonWidth
        height              :   parent.height * controlsHeight
        text                :   "Sensörü Ekle"
        anchors.right       :   parent.right
        fillColor           :   "white"
        textColor           :   "black"
        textFontHeight      :   0.45
        borderColor         :   "white"
        mouseArea.onClicked :   {
            if(sensorNameTextControl.text == ""){
                Global.displayMessage("Sensor ismini giriniz")
                return
            }

            addSensor({ sensorName  :   sensorNameTextControl.text , pin    :   analogIdxControl.value , period     :   periodControl.value , unit  :   unitControl.text})
        }
    }
}
