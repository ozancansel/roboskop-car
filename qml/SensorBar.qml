import QtQuick 2.0
import QtSensors 5.3

Item {


    property real   midVal      :   0
    property real   minimumVal      :   -100
    property real   maximumVal      :   100
    property real   value           :   50
    property real   leftPortion     :   getPortion(Math.abs(minimumVal - midVal))
    property real   rightPortion    :   1 - leftPortion
    property Component  valueBarComp   :   valueBarComponent
    property Component  backgroundComp  :   backgroundComponent

    function    getPortion(val){
        return val / (Math.abs(minimumVal) + Math.abs(maximumVal))
    }

    id                              :   container

    Component{
        id  :   valueBarComponent
        Rectangle   {
            color       :   "green"
        }
    }

    Component{
        id      :   backgroundComponent
        Rectangle{
            color       :   "red"
        }
    }

    Loader{
        property var target :   parent
        id              :   compLoader
        sourceComponent :   valueBarComp
        width           :   parent.width * getPortion(Math.abs(value - midVal))
        x               :   {
            if(value >= midVal) {
                return parent.width * leftPortion
            }
            else if(value <= midVal)
                return parent.width * leftPortion - width
        }
        z               :   backgroundComponentLoader.z + 1

//        SmoothedAnimation on width {

//        }

//        NumberAnimation on width{
//            duration    :   500
//        }

//        Behavior on width{
//            SmoothedAnimation{
//            }
//        }
    }

    Loader{
        id              :   backgroundComponentLoader
        sourceComponent :   backgroundComp
        z               :   1
        anchors.fill    :   parent
        property var target :   parent
    }
}
