import QtQuick 2.6

Rectangle {

    property real headerTextFontHeight      :   0.8
    property real headerTextContainerHeight :   0.3
    property real sensorValueFontHeight     :   0.7
    property real contentWidthPortion       :   0.9
    property real contentTopMarginPortion   :   0.1
    property real borderWidth               :   0.015
    property color borderColor              :   "white"


    property color      headerTextColor         :   "white"
    property color      sensorValueTextColor    :   "white"
    property int        analogIdx               :   -1
    property int        period                  :   200
    property bool       readEnabled             :   false
    property bool       isReading               :   false
    property string     header                  :   "<Başlık>"
    property string     unit                    :   "<unit>"
    property real       value                   :   -1

    property alias      mouseArea           :   area

    id              :   mainRect
    color           :   "transparent"
    border.width    :   mainRect.readEnabled ? Math.min(mainRect.height , mainRect.width) * mainRect.borderWidth : 0
    border.color    :   mainRect.borderColor
    radius          :   height * 0.2

    function    applyReadEnabled(){
        bluetooth.send("{A" + (mainRect.readEnabled ? 1 : 0) + "," + mainRect.analogIdx + "," + mainRect.period + "}")
    }

    states      :   [
        State{
            name    :   "enabled"; when     :   mainRect.readEnabled
        }
    ]

    transitions :   [
        Transition {
            to          : "enabled"

            SequentialAnimation{

                ScaleAnimator{
                    target  :   mainRect
                    from    :   1
                    to      :   0.84
                    duration:   250
                    easing.type :   Easing.InOutCirc
                }

                ScaleAnimator{
                    target  :   mainRect
                    from    :   0.84
                    to      :   1
                    duration:   120
                    easing.type :   Easing.Linear
                }
            }
        }
    ]

    Column{
        id              :   itemsColumn
        anchors.fill    :   mainRect
        anchors.topMargin   :   mainRect.height * mainRect.contentTopMarginPortion
        width           :   mainRect.width    *   mainRect.contentWidthPortion
        anchors.horizontalCenter    :   mainRect.horizontalCenter

        Item{

            height          :   parent.height * mainRect.headerTextContainerHeight
            anchors.left    :   parent.left
            anchors.right   :   parent.right

            Text {
                id                  :   name
                text                :   mainRect.header
                font.pixelSize      :   parent.height * mainRect.headerTextFontHeight
                anchors.centerIn    :   parent
                color               :   mainRect.headerTextColor
            }
        }

        Item{
            id              :   sensorValueContainer
            height          :   parent.height * (1 - mainRect.headerTextContainerHeight)
            anchors.left    :   parent.left
            anchors.right   :   parent.right

            Text {
                id                  :   sensorValueText
                text                :   mainRect.value + " " + mainRect.unit
                font.pixelSize      :   parent.height * mainRect.sensorValueFontHeight
                anchors.centerIn    :   parent
                color               :   mainRect.sensorValueTextColor
            }
        }
    }

    MouseArea{
        id              :       area
        anchors.fill    :       parent
        onClicked       :       {
            mainRect.readEnabled = !mainRect.readEnabled
        }
    }

    Connections {
        target              :   commandInterpreter
        onCommandIncome     :   {
            if(code === 65){
                if(parseInt(commandArgs[0]) === analogIdx){
                    console.log(commandArgs[1])
                    var reading = parseInt(commandArgs[1]);
                    var mv = reading * (5000 / 1023.0)
                    var g = (mv - 2434) / 2.5
                    mainRect.value = Math.floor(g)
                    mainRect.isReading   =   true
                    readingTimeOut.restart()

                    if(!readEnabled)
                        applyReadEnabled()
                }
            }
        }
    }


    Timer{
        id          :   readingTimeOut
        interval    :   period * 3
        onTriggered :   {
            isReading = false
        }
    }

    Timer   {
        id          :   readCheck
        interval    :   1000
        running     :   readEnabled && !isReading
        repeat      :   true
        onTriggered :   {

            //Eğer ki okuma başlamamışsa veya kesilmişse tekrar başlatılıyor
            if(!isReading)
                applyReadEnabled()
        }
    }
}
