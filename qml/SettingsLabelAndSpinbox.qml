import QtQuick 2.0
import QtQuick.Controls 2.1

SpinBox {
    id                  :   yellowSpinner
    value               :   800
    from                :   0
    to                  :   20000
    stepSize            :   200
    font.pixelSize      :   height  *   slidersValueTextFontSize
    up.indicator.width  :   width * 0.2
    down.indicator.width: width * 0.2
}
