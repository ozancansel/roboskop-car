import QtQuick 2.0
import QtQuick.Controls 2.1

Item {

    //Width
    property real textWidthPortion          :   0.5
    property real upIndicatorPortion        :   0.2
    property real downIndicatorPortion      :   0.2

    //Height
    property real spinnerheight             :   0.7

    //Font
    property real textFontPixelSize         :   0.45
    property real spinnerTextFontPixelSize   :   0.5


    property int    maxVal                  :   100
    property int    minVal                  :   0
    property int    value                   :   control.value
    property string text                    :   "<Label>"

    //Alias
    property alias labelAlias               :   label
    property alias spinboxAlias             :   spinner

    id              :   control

    Item{
        id              :   textContainer
        anchors.top     :   parent.top
        anchors.bottom  :   parent.bottom
        width           :   parent.width * textWidthPortion

        Text {
            id                      :   label
            text                    :   control.text
            font.pixelSize          :   parent.height * textFontPixelSize
            anchors.verticalCenter  :   parent.verticalCenter
        }
    }

    SpinBox{
        id                      :   spinner
        anchors.top             :   parent.top
        anchors.bottom          :   parent.bottom
        anchors.left            :   textContainer.right
        anchors.right           :   parent.right

        from                    :   minVal
        to                      :   maxVal
        font.pixelSize          :   height * spinnerTextFontPixelSize
        value                   :   control.value
        up.indicator.width      :   width * upIndicatorPortion
        down.indicator.width    :   width * downIndicatorPortion

    }


    Connections{
        target      :   spinner
        onValueChanged      :   {
            control.value = spinner.value
        }
    }
}
