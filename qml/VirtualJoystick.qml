import QtQuick 2.1

Item {
    id          :   root
    property real   joystickX   :   0
    property real   joystickY   :   0
    property bool   active      :   false
    readonly property real  joystickWidth       :   0.4

    function    calculateMagnitude(){
        var w   =   joystickX * joystickX
        var h   =   joystickY * joystickY
        return Math.sqrt(w + h)
    }

    Rectangle {
        id      :   joystick
        radius  :   height * 0.5
        width   :   Math.min(parent.height , parent.width)
        height  :   width
        anchors.centerIn    :   parent

        property real angle : 0
        property real distance : 0


        ParallelAnimation {
            id: returnAnimation
            NumberAnimation { target: thumb.anchors; property: "horizontalCenterOffset";
                to: 0; duration: 200; easing.type: Easing.OutSine }
            NumberAnimation { target: thumb.anchors; property: "verticalCenterOffset";
                to: 0; duration: 200; easing.type: Easing.OutSine }
        }

        MouseArea {

            id: mouse

            property real fingerAngle : Math.atan2(mouseX, mouseY)
            property int mcx : mouseX - width * 0.5
            property int mcy : mouseY - height * 0.5
            property bool fingerInBounds : fingerDistance2 < distanceBound2
            property real fingerDistance2 : mcx * mcx + mcy * mcy
            property real distanceBound : width * 0.5 - thumb.width * 0.5
            property real distanceBound2 : distanceBound * distanceBound

            property double signal_x : (mouseX - joystick.width/2) / distanceBound
            property double signal_y : -(mouseY - joystick.height/2) / distanceBound

            anchors.fill: parent

            onPressed           :   {
                returnAnimation.stop();
                active          =   true
            }

            onReleased: {
                returnAnimation.restart()
                joystickX   =   0
                joystickY   =   0
                active      =   false
            }

            onPositionChanged: {
                if (fingerInBounds) {
                    thumb.anchors.horizontalCenterOffset = mcx
                    thumb.anchors.verticalCenterOffset = mcy
                } else {
                    var angle = Math.atan2(mcy, mcx)
                    thumb.anchors.horizontalCenterOffset = Math.cos(angle) * distanceBound
                    thumb.anchors.verticalCenterOffset = Math.sin(angle) * distanceBound
                }

                // Fire the signal to indicate the joystick has moved
                angle = Math.atan2(signal_y, signal_x)

                if(fingerInBounds) {
                    joystickX   =   Math.cos(angle) * Math.sqrt(fingerDistance2) / distanceBound
                    joystickY   =   Math.sin(angle) * Math.sqrt(fingerDistance2) / distanceBound
                } else {
                    joystickX   =   Math.cos(angle) * 1
                    joystickY   =   Math.sin(angle) * 1
                }
            }
        }

        Rectangle {
            id                  :   thumb
            radius              :   parent.height * 0.5
            width               :   parent.width * joystickWidth
            height              :   width
            color               :   Qt.rgba((calculateMagnitude() * calculateMagnitude()) , 1 - (calculateMagnitude() * calculateMagnitude()), 0 , 1)
            anchors.centerIn    :   parent
            z                   :   2
        }

        Rectangle{
            x                   :   parent.width / 2
            y                   :   parent.width / 2
            width               :   {
                var sX  =   joystickX * joystick.width * 0.5
                var sY  =   joystickY * joystick.height * 0.5
                var w   =   sX * sX
                var h   =   sY * sY
                Math.sqrt((w + h) * 0.9)
            }
            height      :   parent.height * 0.023
            radius      :   height / 2
            color       :   Qt.rgba((calculateMagnitude() * calculateMagnitude()) , 1 - (calculateMagnitude() * calculateMagnitude()), 0 , 1)
            rotation    :   joystickX < 0 ? -Math.atan(joystickY / joystickX ) * 180 / Math.PI -180 : -Math.atan(joystickY / joystickX ) * 180 / Math.PI
            transformOrigin :   Item.Left
            antialiasing:   true
        }
    }
}
