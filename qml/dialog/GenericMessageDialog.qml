import QtQuick 2.0
import QtQuick.Controls 2.1

Dialog {
    property string     message     :   ""

    id          :   messageDialogCtl
    modal       :   true
    x           :   (parent.width / 2) - (width / 2)
    y           :   (parent.height / 2) - (height / 2)


    header      :   Item{
        implicitHeight      :   40
        implicitWidth       :   messageDialogCtl.width

        Rectangle{
            id      :   back
            anchors.fill    :   parent
            color   :   "#075688"
            opacity :   0.6
        }
        Text {
            id      :   titleText
            text    :   ":: " + messageDialogCtl.title
            anchors.verticalCenter  :   parent.verticalCenter
            x       :   20
            color   :   "#FFF8DC"
            font.pointSize: 15
        }
    }

    contentItem :    Item   {
        implicitWidth   :   messageText.width + 50
        implicitHeight  :   messageText.height + 10

        Text {
            id                  :   messageText
            text                :   message
            font.pointSize      :   12
            color               :   "white"
            z                   :   2
            anchors.centerIn    :   parent
        }
    }

    footer      :   DialogButtonBox {
        id              :   dialogBox
        standardButtons :   messageDialogCtl.standardButtons
        position        :   DialogButtonBox.Footer
        alignment       :   Qt.AlignTop | Qt.AlignRight
        spacing         :   10
        delegate        :   Button{
            implicitHeight  :   40
            implicitWidth   :   100
        }
        background      :   Item {
            id: name
        }
    }

    background  :   Rectangle {
        id          :   img
        anchors.fill:   parent
        z           :   0
        color       :   "green"
    }
}
