pragma Singleton
import QtQuick 2.0

QtObject {

    readonly property int   notWorking  :   0
    readonly property int   active      :   1
    readonly property int   breakdown   :   2

    readonly property int   normalMode   :   100
    readonly property int   manuelMode  :   102

    readonly property int   noneType        :   20
    readonly property int   beginnerType    :   21

}
