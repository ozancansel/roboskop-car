import QtQuick 2.0
import QtBluetooth 5.3
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.0
import QtQuick.Controls 2.0
import "../control"
import "../theme"
import "../enum"
import "../"

Rectangle {


    property alias      dialogWidth         :   container.width
    property alias      dialogHeight        :   container.height
    property double     textHeight          :   0.35
    property double     headerTextHeight    :   0.40

    readonly property real buttonsContainerBottomMargin         :   0.9

    signal  connected()
    signal  disconnected()

    function    connectTo(btAdress){
        bluetooth.connectToBt(btAdress)
    }

    function    startScan(){
        bluetoothModel.clear()
        bluetooth.startScan()
    }

    id                  :   container
    color               :   "#383838"

    ListModel{
        id      :   bluetoothModel
    }

    Toast   {
        id          :   toast
        container   :   parent
    }

    Item{
        width               :   parent.width * 0.6
        height              :   parent.height * 0.6
        anchors.centerIn    :   parent
        z                   :   2

        Text {
            id              :   statusText
            text            :   bluetooth.state == BluetoothStateEnum.discovering ? "Cihaz aranıyor..." : "Cihaz arayın"
            font.pixelSize  :   parent.height * 0.4
            anchors.centerIn:   parent
            color           :   "white"
        }

        MouseArea{
            id              :   mouseArea
            anchors.fill    :   parent
            onClicked       :   startScan()
        }

        visible             :   !bluetoothModel.count
        enabled             :   !bluetoothModel.count
    }

    ListView {

        id                  :   bluetoothListView
        model               :   bluetoothModel
        anchors.top         :   parent.top
        anchors.bottom      :   controlButtonsContainer.top
        height              :   parent.height * 0.8
        width               :   parent.width
        spacing             :   SizeBinding.mm(3)

        delegate :  Rectangle {

            id                      :   btListItemContainer
            width                   :   bluetoothListView.width
            height                  :   bluetoothListView.height * 0.2
            scale                   :   ListView.view.currentIndex === index ? 1.5 : 0.5
            color                   :   "transparent"

            Text {
                id                  :   bluetoothNameText
                text                :   name + " - " + address
                anchors.centerIn    :   parent
                font.pixelSize      :   parent.height * textHeight
                color               :   "white"
            }

            MouseArea {
                anchors.fill        :   parent
                onClicked           :   bluetoothListView.currentIndex = index
            }

            Behavior on scale {                    // added to smooth the scaling
                NumberAnimation {
                    duration        :   150
                }
            }
        }

        header: Component{
            id          :   bluetoothHeading

            Rectangle{
                width               :   bluetoothListView.width
                height              :   bluetoothListView.height * 0.2
                color               :   "transparent"
                Text {
                    id              :   name
                    text            :   qsTr("Bluetooth Listesi")
                    font.pointSize  :   parent.height * headerTextHeight
                    anchors.centerIn:   parent
                    color           :   "white"
                }
            }
        }

        highlight                   :   Rectangle{color : "steelblue"}
        preferredHighlightBegin     :   0
        preferredHighlightEnd       :   delegate.width
        highlightRangeMode          :   ListView.ApplyRange
        cacheBuffer                 :   400
    }


    Rectangle {

        id                      :   controlButtonsContainer
        anchors.bottom          :   parent.bottom
        width                   :   SizeBinding.size(parent.width * 0.45 , SizeBinding.mm(180) , SizeBinding.mm(70))
        height                  :   parent.height * 0.2
        anchors.horizontalCenter:   bluetoothListView.horizontalCenter
        color                   :   "transparent"

        GezginButton{
            id                      :   scanButton
            height                  :   parent.height * 0.6
            width                   :   parent.width * 0.5
            anchors.verticalCenter  :   parent.verticalCenter
            text                    :   "Tara"
            onClicked         :   {
                startScan()
            }
        }

        GezginButton{
            id                      :   connectButton
            width                   :   scanButton.width
            height                  :   scanButton.height
            anchors.left            :   scanButton.right
            anchors.leftMargin      :   SizeBinding.mm(6)
            text                    :   "Bağlan"
            anchors.verticalCenter  :   parent.verticalCenter
            onClicked         :   {
                if(bluetoothListView.currentIndex < 0)
                    return

                connectTo(bluetoothModel.get(bluetoothListView.currentIndex).address)
            }
        }
    }

    Connections{
        target                      :   bluetooth
        onDeviceDiscovered          :   {
            bluetoothModel.append({"name" : name , "address" : address})
            toast.displayMessage("Yeni cihaz bulundu. " + address)
        }
        onStateChanged              :   {
            if(bluetooth.state == BluetoothStateEnum.connected){
                toast.displayMessage("Bluetooth bağlantısı kuruldu.")
                connected()
            }
        }
    }
}

