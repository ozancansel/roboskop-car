import QtQuick 2.0

Item {

    signal  toastMessageRequired(string message)

    function displayMessage(msg){
        toastMessageRequired(msg)
    }

}
