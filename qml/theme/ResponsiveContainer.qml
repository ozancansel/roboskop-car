import QtQuick 2.0

Item {

    readonly property real scaleFactorX :   width / 1200
    readonly property real scaleFactorY :   height /675

    function getScaledX(val){
        var res = Math.ceil(val * scaleFactorX)
        if(res < 0)
            res = 0
        return res
    }

    function getScaledY(val){

        var res =  Math.ceil(val * scaleFactorY)

        if(res < 0)
            res = 0
        return res
    }

}
