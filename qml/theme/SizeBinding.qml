pragma Singleton
import QtQuick 2.0
import QtQuick.Window 2.0

QtObject {
    function size(normalWidth , maxWidth , minWidth){
        if(normalWidth > maxWidth)
            return maxWidth

        if(normalWidth < minWidth)
            return minWidth

        return normalWidth
    }

    function mm(mm){
        return  Screen.pixelDensity * mm
    }

    function screenWidth(){
        return Screen.width
    }

    function screenHeight(){
        return Screen.height
    }

    function sizePortion(cNum , spacing){
        var res = (1 - ((cNum -1) * spacing)) / cNum
        return res
    }

    function    calculateTextPixelSize(txt , pixelSize , maxHeight , maxWidth){
        var length = txt.length
        var preferredWidth = length *  pixelSize

        if(preferredWidth > maxWidth){
            return maxWidth / preferredWidth * pixelSize
        }

        return pixelSize
    }
}
