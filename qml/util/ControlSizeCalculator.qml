import QtQuick 2.0

Item {

    property real   widthPortion                :   0
    property real   ratio                       :   0
    property real   spacingRatio                :   0
    readonly property real calculatedWidth      :   parent.width * widthPortion
    readonly property real calculatedHeight     :   calculatedWidth * (1 / ratio)
    readonly property real spacingWidth         :   calculatedWidth * spacingRatio

}
