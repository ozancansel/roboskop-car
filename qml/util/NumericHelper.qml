pragma Singleton
import QtQuick 2.0

QtObject {

    function map(x , in_min , in_max , out_min , out_max){
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

    function sign(val){
        if(val > 0)
            return 1
        else if(val === 0)
            return 0
        else
            return -1
    }
}
