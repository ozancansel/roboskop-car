#include "robogezginconfig.h"
#include <QFile>
#include <QTextStream>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include <QtQml>

void RoboGezginConfig::registerQmlType(){
    qmlRegisterType<RoboGezginConfig>("GezginBot", 1 , 0 , "RoboGezginConfig");
}

RoboGezginConfig::RoboGezginConfig()
{
    setSensivity(50);
    setDeadzone(0);
    setMaxForwardThrottle(100);
    setMaxRearThrottle(100);
    setMaxSteering(100);
    _timer.setInterval(1000);
    _timer.setSingleShot(true);

    connect(&_timer , SIGNAL(timeout()) , this , SLOT(saveTimerTick()));

    _filePath   =   "robogezgin-config.json";
    //    _requiredLabels << SETTINGS_SENSIVITY_LABEL << SETTINGS_DEADZONE_LABEL << SETTINGS_MAX_FORWARD_THROTTLE_LABEL << SETTINGS_MAX_REAR_THROTTLE_LABEL << SETTINGS_MAX_STEERING_LABEL;

    _requiredFields.append(ConfigField { QString(SETTINGS_SENSIVITY_LABEL) , QVariant(50) });
    _requiredFields.append(ConfigField { QString(SETTINGS_DEADZONE_LABEL) , QVariant(0)});
    _requiredFields.append(ConfigField { QString(SETTINGS_MAX_FORWARD_THROTTLE_LABEL) , QVariant(100) });
    _requiredFields.append(ConfigField { QString(SETTINGS_MAX_REAR_THROTTLE_LABEL) , QVariant(100)});
    _requiredFields.append(ConfigField { QString(SETTINGS_MAX_STEERING_LABEL) , QVariant(100)});

    loadSettings();
}

int RoboGezginConfig::sensivity(){
    return  _sensivity;
}

int RoboGezginConfig::deadzone(){
    return _deadzone;
}

int RoboGezginConfig::maxForwardThrottle(){
    return _maxForwardThrottle;
}

int RoboGezginConfig::maxRearThrottle(){
    return  _maxRearThrottle;
}

int RoboGezginConfig::maxSteering(){
    return _maxSteering;
}

int RoboGezginConfig::accelerationMagnitude(){
    return _accelerationMagnitude;
}

void RoboGezginConfig::setSensivity(int val){

    if(_sensivity != val){
        _timer.stop();
        _timer.start();
    }

    _sensivity = val;

    emit sensivityChanged();
}

void RoboGezginConfig::setDeadzone(int val){
    if(_deadzone != val){
        _timer.stop();
        _timer.start();
    }

    _deadzone = val;

    emit deadzoneChanged();
}

void RoboGezginConfig::setMaxForwardThrottle(int val){

    if(_maxForwardThrottle != val){
        _timer.stop();
        _timer.start();
    }

    _maxForwardThrottle = val;

    emit maxForwardThrottleChanged();
}

void RoboGezginConfig::setMaxRearThrottle(int val){

    if(_maxRearThrottle != val){
        _timer.stop();
        _timer.start();
    }

    _maxRearThrottle = val;

    emit maxRearThrottleChanged();
}

void RoboGezginConfig::setMaxSteering(int val){
    if(_maxSteering != val){
        _timer.stop();
        _timer.start();
    }

    _maxSteering = val;

    emit maxSteeringChanged();
}

void RoboGezginConfig::setAccelerationMagnitude(int val){
    if(_accelerationMagnitude != val){
        _timer.stop();
        _timer.start();
    }

    _accelerationMagnitude = val;

    emit accelerationMagnitudeChanged();
}

void RoboGezginConfig::saveSettings(){

    QJsonObject obj;

    obj[SETTINGS_SENSIVITY_LABEL]       =   sensivity();
    obj[SETTINGS_DEADZONE_LABEL]        =   deadzone();
    obj[SETTINGS_MAX_FORWARD_THROTTLE_LABEL]        =   maxForwardThrottle();
    obj[SETTINGS_MAX_REAR_THROTTLE_LABEL]           =   maxRearThrottle();
    obj[SETTINGS_MAX_STEERING_LABEL]                =   maxSteering();


    QFile   settingsFile(_filePath);

    settingsFile.open(QIODevice::WriteOnly);

    QTextStream fStream(&settingsFile);

    fStream << QJsonDocument(obj).toJson();

    settingsFile.close();

}

void RoboGezginConfig::loadSettings(){

    createIfNotExists();
    checkSettingsFile();

    QFile   settingsFile(_filePath);

    if(!settingsFile.open(QIODevice::ReadWrite)){
        qDebug() << "Ayar dosyası açılamadı";

        return;
    }

    QTextStream fStream(&settingsFile);
    QString     jsonStr =   fStream.readAll();


    QJsonDocument   doc =   QJsonDocument::fromJson(jsonStr.toUtf8());
    QJsonObject     settingsJson = doc.object();

    //Dosya kapatılıyor
    settingsFile.close();

    setSensivity(settingsJson[SETTINGS_SENSIVITY_LABEL].toInt());
    setDeadzone(settingsJson[SETTINGS_DEADZONE_LABEL].toInt());
    setMaxForwardThrottle(settingsJson[SETTINGS_MAX_FORWARD_THROTTLE_LABEL].toInt());
    setMaxRearThrottle(settingsJson[SETTINGS_MAX_REAR_THROTTLE_LABEL].toInt());
    setMaxSteering(settingsJson[SETTINGS_MAX_STEERING_LABEL].toInt());
}

void RoboGezginConfig::checkSettingsFile(){
    QFile   settingsFile(_filePath);

    //Dosya açılıyor
    if(!settingsFile.open(QIODevice::ReadWrite)){
        throw "Bilinmeyen bir hata oluştu";
    }

    QTextStream     fStream(&settingsFile);
    QString         jsonStr = fStream.readAll();

    QJsonDocument   doc = QJsonDocument::fromJson(jsonStr.toUtf8());
    QJsonObject     settingsJson = doc.object();

    bool        changed = false;

    foreach (ConfigField field, _requiredFields) {
        if(settingsJson[field.label].isNull() || settingsJson[field.label].isUndefined()){
            settingsJson[field.label] = field.defaultVal.toInt();
            changed = true;
        }
    }

    if(changed){
        fStream << QJsonDocument(settingsJson).toJson();
    }

    settingsFile.close();
}

bool RoboGezginConfig::createIfNotExists(){
    QFile   settingsFile(_filePath);

    if(!settingsFile.open(QIODevice::ReadWrite)){
        qDebug() << "Ayar dosyası oluşturulamadı.";

        return false;
    }

    settingsFile.close();

    return true;
}

void RoboGezginConfig::saveTimerTick(){
    saveSettings();
}
