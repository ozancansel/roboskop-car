#ifndef ROBOGEZGINCONFIG_H
#define ROBOGEZGINCONFIG_H

#include <QObject>
#include <QTimer>
#include <QVariant>
#define     SETTINGS_SENSIVITY_LABEL        "sensivity"
#define     SETTINGS_DEADZONE_LABEL         "deadzone"
#define     SETTINGS_MAX_FORWARD_THROTTLE_LABEL     "max-forward-th"
#define     SETTINGS_MAX_REAR_THROTTLE_LABEL        "max-rear-th"
#define     SETTINGS_MAX_STEERING_LABEL             "max-steering"

class RoboGezginConfig : public QObject
{

    Q_OBJECT

    Q_PROPERTY(int sensivity READ sensivity WRITE setSensivity NOTIFY sensivityChanged)
    Q_PROPERTY(int deadzone READ deadzone WRITE setDeadzone NOTIFY deadzoneChanged)
    Q_PROPERTY(int maxForwardThrottle READ maxForwardThrottle WRITE setMaxForwardThrottle NOTIFY maxForwardThrottleChanged)
    Q_PROPERTY(int maxRearThrottle READ maxRearThrottle WRITE setMaxRearThrottle NOTIFY maxRearThrottleChanged)
    Q_PROPERTY(int maxSteering READ maxSteering WRITE setMaxSteering NOTIFY maxSteeringChanged)
    Q_PROPERTY(int accelerationMagnitude READ accelerationMagnitude WRITE setAccelerationMagnitude NOTIFY accelerationMagnitudeChanged)

public:


    struct ConfigField{
        QString     label;
        QVariant    defaultVal;
    };

    RoboGezginConfig();

    static   void   registerQmlType();
    int     sensivity();
    int     deadzone();
    int     maxForwardThrottle();
    int     maxRearThrottle();
    int     maxSteering();
    int     accelerationMagnitude();
    void    setSensivity(int val);
    void    setDeadzone(int val);
    void    setMaxForwardThrottle(int val);
    void    setMaxRearThrottle(int val);
    void    setMaxSteering(int val);
    void    setAccelerationMagnitude(int val);

public slots:

    void    saveSettings();
    void    loadSettings();

private slots:

    void    saveTimerTick();

signals:

    void    sensivityChanged();
    void    deadzoneChanged();
    void    maxForwardThrottleChanged();
    void    maxRearThrottleChanged();
    void    maxSteeringChanged();
    void    accelerationMagnitudeChanged();


private:

    int     _sensivity;
    int     _deadzone;
    int     _maxForwardThrottle;
    int     _maxRearThrottle;
    int     _maxSteering;
    int     _accelerationMagnitude;

    QList<ConfigField>  _requiredFields;

    QString     _filePath;
    QTimer      _timer;

    void    checkSettingsFile();
    bool    createIfNotExists();

};

#endif // ROBOGEZGINCONFIG_H
