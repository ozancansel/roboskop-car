QT += qml quick bluetooth

CONFIG += c++11

SOURCES += main.cpp \
            robogezginconfig.cpp \
            bluetoothutil.cpp \
            serialcommandinterpreter.cpp

HEADERS += robogezginconfig.h \
            bluetoothutil.h \
            serialcommandinterpreter.h

RESOURCES += qml.qrc \
    media.qrc

DISTFILES +=    qml/AccelerometerInput.qml \
                qml/AccelerometerSignalTemplate.qml \
                qml/Arrow.qml \
                qml/CollapsiblePanel.qml \
                qml/ControlButton.qml \
                qml/ControlContainer.qml \
                qml/DrivingCalculater.qml \
                qml/GezginBotSettings.qml \
                qml/GezginBtController.qml \
                qml/GezginConsoleVisual.qml \
                qml/GezginPov.qml \
                qml/LabelWithTextEdit.qml \
                qml/LineFollowing.qml \
                qml/SensorAddControl.qml \
                qml/SensorBar.qml \
                qml/SensorSignalTemplate.qml \
                qml/settingsLabelAndSpinbox.qml \
                qml/settingsSpinBoxWithLabel.qml \
                qml/VirtualJostick.qml \
                qml/form/FormPage.qml \
                qml/form/BluetoothDialog.qml \
                qml/control/Toast.qml \
                qml/util/NumericHelper.qml \
                qml/VirtualJoystick.qml \
                main.qml \
    qml/GezginButton.qml \
    qml/util/ControlSizeCalculator.qml \
    qml/dialog/GenericDialog.qml \
    qml/dialog/GenericMessageDialog.qml

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
