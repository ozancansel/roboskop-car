#ifndef SERIALCOMMANDINTERPRETER_H
#define SERIALCOMMANDINTERPRETER_H

#include <QObject>
#include <QIODevice>
#include "iroboskopcommandinterpreter.h"

class SerialCommandInterpreter : public QObject
{

    Q_OBJECT

public:

    enum        SerialCommandStatus { None = 0, Start = 1};
    SerialCommandInterpreter(QObject    *parent = NULL);
    SerialCommandInterpreter(QIODevice *device , QObject *parent);
    void        listen(QIODevice *device);

signals:

    void    commandIncome(int code , QStringList commandArgs);

private slots:

    void        onReadyRead();

private:


    SerialCommandStatus         _status;
    QByteArray                  _buffer;
    QIODevice                   *_device;
    QByteArray                  _commandStart;
    QByteArray                  _commandEnd;


    void            initializeDefaults();
};

#endif // SERIALCOMMANDINTERPRETER_H
